package com.dercd.cdio;

public interface IncommingConnectionDelegate
{
	public void incommingConnection(CDIncommingConnection cdic);
}

package com.dercd.cdio;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.function.Consumer;

public class LogPrinter
{
	public Consumer<String> out;
	
	protected void println(String s)
	{
		printlnRaw(s);
	}
	protected void printlnRaw(String s)
	{
		if(this.out != null)
			this.out.accept(s);
	}
	
	protected void printException(Throwable t)
	{
		if(this.out != null)
		{
			StringWriter w = new StringWriter();
			t.printStackTrace(new PrintWriter(w));
			for(String line : w.toString().split("\\r?\\n|\\r"))
				this.out.accept(line);
		}
	}
}

package com.dercd.cdio;

public enum OptionMask
{
	RAW((byte) 0x01);
	
	
	public final byte value;
	
	private OptionMask(byte value)
	{
		this.value = value;
	}
	
	public byte getValue()
	{
		return this.value;
	}
}

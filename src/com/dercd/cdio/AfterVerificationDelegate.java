package com.dercd.cdio;

public interface AfterVerificationDelegate
{
	public void afterVerification(CDProtocol cdp);
}

package com.dercd.cdio;

public interface ConnectionCloseDelegate
{
	public void onClosed(CDConnection connection, CloseReason reason);
}

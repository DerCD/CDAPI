package com.dercd.cdio;

public enum CloseReason
{
    OC_CON_BUILD_UP_FAILED,
    OC_CON_CRASHED,
    OC_CON_CLOSED,
    IC_CON_CRASHED,
    IC_CON_CLOSED,
    MANUAL,
    THREAD_INTERRUPTED
}

package com.dercd.cdio;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.function.Consumer;

public class CDPortListener extends LogPrinter
{
	public Thread t;
	protected ServerSocket listener;
	protected int port;
	public byte[] keyWordIn;
	public byte[] keyWordOut;
	protected ArrayList<CDIncommingConnection> openedConnections = new ArrayList<CDIncommingConnection>();
	protected int cleanupCount = 100;
	public IncommingConnectionDelegate onIncommingConnection;
	public CDICCreator creator = (s, keyWordIn, keyWordOut) -> new CDIncommingConnection(s, keyWordIn, keyWordOut, null, null);
	public InetAddress ipaddress;
	public ThreadGroup threadGroup;
	
	public Consumer<String> out;
	
	public CDPortListener(int port)
	{
		this(port, null, null, null, null, null, null);
	}
	public CDPortListener(int port, InetAddress ipaddress, IncommingConnectionDelegate onIncommingConnection, CDICCreator creator, byte[] keyWordIn, byte[] keyWordOut, ThreadGroup threadGroup)
	{
		this.port = port;
		this.keyWordIn = keyWordIn;
		this.keyWordOut = keyWordOut;
		if(creator != null)
			this.creator = creator;
		this.onIncommingConnection = onIncommingConnection;
		this.threadGroup = threadGroup;
		try
		{
			this.ipaddress = ipaddress != null ? ipaddress : InetAddress.getByAddress(new byte[] { 0, 0, 0, 0 });
		}
		catch (UnknownHostException x)
		{
			//Won't happen
			printException(x);
		}
	}
	
	public void run()
	{
		if(this.t == null || this.t != Thread.currentThread())
		{
			if(this.threadGroup == null)
				this.t = new Thread(() -> { run(); });
			else
				this.t = new Thread(this.threadGroup, () -> { run(); });
			this.t.setName("CDPortListener-Thread");
			this.t.start();
			return;
		}
		try
		{
			while (true)
			{
				println("Start listening");
				this.listener = new ServerSocket(this.port, 20, this.ipaddress);
				while (true)
					startConnection();
			}
		}
		catch (IOException x)
		{
			printException(x);
		}
		finally
		{
			close();
		}
	}
	
	public void interrupt()
	{
		close();
		if (this.t != null)
			this.t.interrupt();
	}

	public void close()
	{
		try
		{
			this.listener.close();
		}
		catch(Throwable t)
		{
			printException(t);
		}
	}
	public void closeConnections()
	{
		for(CDIncommingConnection cdic : this.openedConnections)
		{
			try
			{
				if(cdic.isActive())
					cdic.close(CloseReason.MANUAL);
			}
			catch (Throwable t)
			{
				printException(t);
			}
		}
	}
	
	public void startConnection() throws IOException
	{
		if (this.openedConnections.size() >= this.cleanupCount)
			removeClosedConnections();
		CDIncommingConnection cdic = this.creator.create(this.listener.accept(), this.keyWordIn, this.keyWordOut);
		println("Accepted connection");
		synchronized (this.openedConnections)
		{
			this.openedConnections.add(cdic);
		}
		if (this.onIncommingConnection != null)
			this.onIncommingConnection.incommingConnection(cdic);
		cdic.run();
	}

	public ArrayList<CDIncommingConnection> getOpenedConnections()
	{
		return new ArrayList<CDIncommingConnection>(this.openedConnections);
	}

	public void removeClosedConnections()
	{
		synchronized (this.openedConnections)
		{
			this.openedConnections.removeIf((p) -> !p.isActive());
		}
	}
	
	@Override
	protected void println(String s)
	{
		printlnRaw("PortListener[" + this.ipaddress.toString() + ":" + this.port + "]: " + s);
	}
	
	public boolean isActive()
	{
		return this.t != null && this.t.isAlive() && this.listener != null;
	}
	
	public int getPort()
	{
		return this.port;
	}
	
	public InetAddress getIPAddress()
	{
		return this.ipaddress;
	}
}
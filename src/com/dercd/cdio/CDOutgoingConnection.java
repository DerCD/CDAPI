package com.dercd.cdio;

import java.net.Socket;

public class CDOutgoingConnection extends CDConnection
{
	private int port;
	private String ip;
	public ConnectionOpenDelegate onConnected;
	private boolean exc = false;
	private boolean connected;
	public ThreadGroup threadGroup;
	
	public CDOutgoingConnection(String ip, int port)
	{
		this(ip, port, null, null, null, null, null);
	}
	public CDOutgoingConnection(String ip, int port, byte[] keyWordIn, byte[] keyWordOut, CDProtocol cdp, ConnectionOpenDelegate onConnected, ThreadGroup threadGroup)
	{
		super(keyWordIn, keyWordOut);
		this.ip = ip;
		this.port = port;
		this.protocol = cdp;
		this.connected = false;
		this.onConnected = onConnected;
		this.threadGroup = threadGroup;
	}
	
	public void run()
	{
		if(this.t != Thread.currentThread())
		{
			this.t = new Thread(this.threadGroup, () -> { run(); });
			this.t.setName("CDOC-Thread");
			this.t.start();
			return;
		}
		try
		{
			while (true)
			{
				try
				{
					openConnection();
					this.connected = true;
					if (this.onConnected != null)
						this.onConnected.onConnected(this);
				}
				catch (Throwable t)
				{
					printException(t);
					this.exc = true;
					close(CloseReason.OC_CON_BUILD_UP_FAILED);
				}
				if(this.exc)
				{
					this.exc = false;
					Thread.sleep(10000);
					continue;
				}
				try
				{
					listen();
					close(CloseReason.OC_CON_CLOSED);
				}
				catch(Throwable t)
				{
					printException(t);
					close(CloseReason.OC_CON_CRASHED);
				}
				Thread.sleep(3000);
			}
		}
		catch(InterruptedException x)
		{}
	}
	
	protected void openConnection() throws Exception
	{
		println("Opening connection to " + this.ip + ":" + this.port);
		this.socket = new Socket(this.ip, this.port);
		println("Connection to " + this.ip + ":" + this.port + " opened (" + this.socket.isConnected() + ")");
		this.inputStream = this.socket.getInputStream();
		this.outputStream = this.socket.getOutputStream();
		if (this.socket.isConnected())
			this.address = this.socket.getInetAddress();
//		if (isProtocolSet())
//		{
//			println("Protocol already exits. Reseting verification");
//			this.protocol.resetVerification();
//		}
//		else
//		{
			println("Creating protocol");
			createProtocol();
//		}
		sendKeyword();
	}
	
	@Override
	public void close(CloseReason reason)
	{
		try
		{
			this.connected = false;
			this.socket.close();
		}
		catch (Throwable t)
		{
			printException(t);
		}
		println("Closed (" + reason.name() + ")");
		callOnClosed(reason);
	}
	
	@Override
	public boolean isAlive()
	{
		return super.isAlive() && this.connected;
	}
	
	public boolean isConnected()
	{
		return this.connected;
	}
} 
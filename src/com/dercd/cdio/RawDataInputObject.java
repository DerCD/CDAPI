package com.dercd.cdio;

public interface RawDataInputObject
{
	public void input(byte[] data, boolean end, int start, int length);
}

package com.dercd.bukkit.cdapi.events;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.tools.collection.PresetMap;

/**
 * A class which can be used for Events,
 * <p>
 * which need a feedback if it was successfully processed
 * 
 * @author CD
 */
public class ActionEvent extends Event
{
	private static final HandlerList handlers = new HandlerList();
	private Map<CDPlugin, Boolean> success = new PresetMap<CDPlugin, Boolean>(false);
	public boolean wasFired = false;

	@Override
	public HandlerList getHandlers()
	{
		return handlers;
	}

	public static HandlerList getHandlerList()
	{
		return handlers;
	}

	/**
	 * @return A set of all CDPlugin which catched this ActionEvent
	 */
	public Set<CDPlugin> getPlugins()
	{
		return this.success.keySet();
	}

	/**
	 * @param plugin The CDPlugin the success-state should be set for
	 * @param success The success-state that should be set
	 */
	public void setSuccess(CDPlugin plugin, boolean success)
	{
		this.success.put(plugin, success);
	}

	/**
	 * @param plugin The CDPlugin which success-state-set-state should be return
	 * @return If the success-state for the given CDPlugin is set
	 */
	public boolean isSuccessSet(CDPlugin plugin)
	{
		return this.success.containsKey(plugin);
	}

	/**
	 * @param plugin The CDPlugin which success-state should be returned
	 * @return The success-state of the given CDPlugin
	 */
	public boolean getSuccess(CDPlugin plugin)
	{
		return this.success.get(plugin);
	}

	/**
	 * @return A Collection containing all set success-states of this
	 *         ActionEvent
	 */
	public Collection<Boolean> getSuccess()
	{
		return this.success.values();
	}
}

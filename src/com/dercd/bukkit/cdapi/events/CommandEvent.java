package com.dercd.bukkit.cdapi.events;

import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.dercd.bukkit.cdapi.exceptions.CDInvalidArgsException;
import com.dercd.bukkit.cdapi.exceptions.CDNoPermissionException;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

/**
 * A class whose objects are containing a dispatched command and its sender
 * 
 * @author CD
 */
public class CommandEvent extends Event implements Cancellable
{
	private static final HandlerList handlers = new HandlerList();
	private boolean cancelled;
	private Command command;
	private CommandSender sender;
	private String[] args;
	private String strArgs;

	/**
	 * @param command The command which was dispatched
	 * @param sender The CommandSender which dispatched the command
	 * @param args The arguments given by
	 */
	public CommandEvent(Command command, CommandSender sender, String[] args)
	{
		this.cancelled = false;
		this.command = command;
		this.sender = sender;
		setArgs(args);
	}

	/**
	 * @return The dispatched command
	 */
	public Command getCommand()
	{
		return this.command;
	}

	/**
	 * @return The sender which dispatched the command
	 */
	public CommandSender getSender()
	{
		return this.sender;
	}

	/**
	 * @return The arguments given by by the sender
	 */
	public String[] getArgs()
	{
		return this.args;
	}

	/**
	 * @return The arguments splitted by spaces
	 */
	public String getStrArgs()
	{
		return this.strArgs;
	}

	/**
	 * @return The command and the arguments splitted by spaces
	 */
	public String getFullCommand()
	{
		return this.getCommand().getName() + (this.strArgs.length() == 0 ? "" : " " + this.strArgs);
	}

	/**
	 * @return The command as string
	 */
	public String getStrCommand()
	{
		return this.getCommand().getName();
	}

	/**
	 * @param args The arguments that should be set
	 */
	public void setArgs(String[] args)
	{
		this.args = args;
		this.strArgs = Var.arrToString(args, 0);
	}

	/**
	 * @param exact The count the arguments length should be checked for
	 * @deprecated Use {@link #validateCount(int)} instead
	 * @throws CDInvalidArgsException
	 */
	@Deprecated
	public void validateArgs(int exact) throws CDInvalidArgsException
	{
		validateCount(exact);
	}

	/**
	 * @param exact The count the arguments length should be checked for
	 * @throws CDInvalidArgsException
	 */
	public void validateCount(int exact) throws CDInvalidArgsException
	{
		validateCount(exact, exact);
	}

	/**
	 * @deprecated Use {@link #validateCount(int,int)} instead
	 * @param min The minimum count the arguments length should be checked for
	 * @param max The maximum count the arguments length should be checked for
	 *        <p>
	 *        -1 is endless
	 * @throws CDInvalidArgsException
	 */
	@Deprecated
	public void validateArgs(int min, int max) throws CDInvalidArgsException
	{
		validateCount(min, max);
	}

	/**
	 * @param min The minimum count the arguments length should be checked for
	 * @param max The maximum count the arguments length should be checked for
	 *        <p>
	 *        -1 is endless
	 * @throws CDInvalidArgsException
	 */
	public void validateCount(int min, int max) throws CDInvalidArgsException
	{
		if ((min != -1 && this.args.length < min) || (max != -1 && this.args.length > max)) throw new CDInvalidArgsException(this.command.getName());
	}

	/**
	 * @param args The regex this command arguments should be matched with
	 *        <p>
	 *        null means this index won't be checked
	 * @throws CDInvalidArgsException
	 */
	public void validateContent(String... args) throws CDInvalidArgsException
	{
		validateCount(args.length);
		for (int i = 0; i < args.length; i++)
			if (args[i] != null && Pattern.matches(args[i], this.args[i])) throw new CDInvalidArgsException(this.command.getName());
	}

	/**
	 * @param perms The permissions the CommandSender have to have,
	 *        <p>
	 *        if he doesn't he will be notified
	 *        <p>
	 *        Checks for CONSOLE will always succeed
	 *        <p>
	 *        A - in front of a permission will negate the result (excepted CONSOLE)
	 * @throws CDNoPermissionException
	 */
	public void validatePerms(String... perms) throws CDNoPermissionException
	{
		validatePerms(true, perms);
	}

	/**
	 * @param notify If the CommandSender should be notified if he doesn't have
	 *        all given permissions
	 * @param perms The permissions the CommandSender have to have,
	 *        <p>
	 *        Checks for CONSOLE will always succeed
	 *        <p>
	 *        A - in front of a permission will negate the result (excepted CONSOLE)
	 * @throws CDNoPermissionException
	 */
	public void validatePerms(boolean notify, String... perms) throws CDNoPermissionException
	{
		for (String perm : perms)
			if (!(this.sender instanceof ConsoleCommandSender) && (perm.startsWith("-") && perm.length() >= 2) ? this.sender.hasPermission(perm.substring(1)) : !this.sender.hasPermission(perm)) throw new CDNoPermissionException(notify);
	}
	
	/**
	 * @param perm The permission the CommandSender have to have,
	 *        <p>
	 *        if he doesn't he will be notified
	 *        <p>
	 *        Checks for CONSOLE will always succeed
	 *        <p>
	 *        A - in front of the permission will negate the result (excepted CONSOLE)
	 * @throws CDNoPermissionException
	 */
	public void validatePerm(String perm) throws CDNoPermissionException
	{
		validatePerm(perm, true);
	}
	
	/**
	 * @param notify If the CommandSender should be notified if he doesn't have
	 *        all given permissions
	 * @param perm The permission the CommandSender have to have,
	 *        <p>
	 *        Checks for CONSOLE will always succeed
	 *        <p>
	 *        A - in front of the permission will negate the result (excepted CONSOLE)
	 * @throws CDNoPermissionException
	 */
	public void validatePerm(String perm, boolean notify) throws CDNoPermissionException
	{
		if (!this.sender.hasPermission(perm)) throw new CDNoPermissionException(notify);
	}
	
	/**
	 * 
	 * @param index The index that should be checked for being an Integer
	 * @throws CDInvalidArgsException
	 */
	public void validateInt(int index) throws CDInvalidArgsException
	{
		try
		{
			if (!Var.ifInt(this.args[index])) throw new CDInvalidArgsException();
		}
		catch (ArrayIndexOutOfBoundsException x)
		{
			throw new CDInvalidArgsException();
		}
	}
	
	/**
	 * 
	 * @param index The indexe that should be checked for being an Integer
	 * @throws CDInvalidArgsException
	 */
	public void validateInt(int... index) throws CDInvalidArgsException
	{
		for (int i : index)
			validateInt(i);
	}
	
	/**
	 * 
	 * @param index The index that should be checked for being a Long
	 * @throws CDInvalidArgsException
	 */
	public void validateLong(int index) throws CDInvalidArgsException
	{
		if (!Var.ifLong(this.args[index])) throw new CDInvalidArgsException();
	}
	
	/**
	 * 
	 * @param index The indexe that should be checked for being a Long
	 * @throws CDInvalidArgsException
	 */
	public void validateLong(int... index) throws CDInvalidArgsException
	{
		for (int i : index)
			validateLong(i);
	}
	
	/**
	 * 
	 * @param index The index that should be checked for being a Short
	 * @throws CDInvalidArgsException
	 */
	public void validateShort(int index) throws CDInvalidArgsException
	{
		if (!Var.ifShort(this.args[index])) throw new CDInvalidArgsException();
	}
	
	/**
	 * 
	 * @param index The indexe that should be checked for being a Short
	 * @throws CDInvalidArgsException
	 */
	public void validateShort(int... index) throws CDInvalidArgsException
	{
		for (int i : index)
			validateShort(i);
	}
	
	/**
	 * 
	 * @param index The index that should be checked for being a Double
	 * @throws CDInvalidArgsException
	 */
	public void validateDouble(int index) throws CDInvalidArgsException
	{
		if (!Var.ifDouble(this.args[index])) throw new CDInvalidArgsException();
	}
	
	/**
	 * 
	 * @param index The indexe that should be checked for being a Double
	 * @throws CDInvalidArgsException
	 */
	public void validateDouble(int... index) throws CDInvalidArgsException
	{
		for (int i : index)
			validateDouble(i);
	}
	
	/**
	 * 
	 * @param index The index that should be checked for being a Float
	 * @throws CDInvalidArgsException
	 */
	public void validateFloat(int index) throws CDInvalidArgsException
	{
		if (!Var.ifFloat(this.args[index])) throw new CDInvalidArgsException();
	}
	
	/**
	 * 
	 * @param index The indexe that should be checked for being a Float
	 * @throws CDInvalidArgsException
	 */
	public void validateFloat(int... index) throws CDInvalidArgsException
	{
		for (int i : index)
			validateFloat(i);
	}
	
	/**
	 * 
	 * @param index The indee that should be checked for being a number
	 * @throws CDInvalidArgsException
	 */
	public void validateNumber(int index) throws CDInvalidArgsException
	{
		if (!Var.ifDouble(this.args[index]) && !Var.ifLong(this.args[index])) throw new CDInvalidArgsException();
	}
	
	/**
	 * 
	 * @param index The indexe that should be checked for being a number
	 * @throws CDInvalidArgsException
	 */
	public void validateNumber(int... index) throws CDInvalidArgsException
	{
		for (int i : index)
			validateNumber(i);
	}
	
	/**
	 * 
	 * @return If the command was sent from CONSOLE
	 */
	public boolean isSenderConsole()
	{
		return this.sender == Bukkit.getConsoleSender();
	}
	
	@Override
	public boolean isCancelled()
	{
		return this.cancelled;
	}
	
	@Override
	public void setCancelled(boolean cancel)
	{
		this.cancelled = cancel;
	}

	@Override
	public HandlerList getHandlers()
	{
		return handlers;
	}

	public static HandlerList getHandlerList()
	{
		return handlers;
	}
}

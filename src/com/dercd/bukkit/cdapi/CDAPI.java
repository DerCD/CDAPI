package com.dercd.bukkit.cdapi;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import com.dercd.bukkit.plugins.CDAPICommand;

/**
 * Main class
 * 
 * @author DerCD
 */
public class CDAPI extends JavaPlugin
{
	private static CDAPI instance;
	private PluginHandler handler = new PluginHandler(this);

	public CDAPI()
	{
		if (instance == null) instance = this;
	}

	/**
	 * Called from Bukkit on Loading
	 */
	@Override
	public void onLoad()
	{
		this.handler.clog.log("Loading CDAPI", this);
		this.handler.load();
	}

	/**
	 * Called from Bukkit on Enabling
	 */
	@Override
	public void onEnable()
	{
		this.handler.clog.log("Beginning Enabling", this);
		this.handler.enable();
		this.handler.log.info("[CDAPI] Plugins enabled");
	}

	/**
	 * Called from Bukkit on Disabling
	 */
	@Override
	public void onDisable()
	{
		this.handler.clog.log("Beginning Disabling", this);
		this.handler.disable();
		this.handler.log.info("[CDAPI] Plugins disabled");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		this.handler.getPlugin(CDAPICommand.class).onCommand(args, sender);
		return true;
	}

	/**
	 * @return The PluginHandler
	 */
	public PluginHandler getHandler()
	{
		return this.handler;
	}

	/**
	 * @return Instance created by Bukkit
	 *         <p>
	 *         (may be slower)
	 */
	public static CDAPI getBukkitInstance()
	{
		return (CDAPI) Bukkit.getPluginManager().getPlugin("CDAPI");
	}

	/**
	 * @return Instance set by Code. Mostly Bukkit-Instance
	 */
	public static CDAPI getInstance()
	{
		return instance;
	}

	/**
	 * Set Instance to get in getInstance()
	 */
	public static void setInstance(CDAPI instance)
	{
		CDAPI.instance = instance;
	}
}

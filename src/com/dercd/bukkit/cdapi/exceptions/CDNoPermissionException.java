package com.dercd.bukkit.cdapi.exceptions;

import org.bukkit.ChatColor;

import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class CDNoPermissionException extends CDCException
{
	private static final long serialVersionUID = 134708763002176638L;
	public boolean notify;
	public String message = Tools.Var.getExclamation(ChatColor.RED) + ChatColor.DARK_RED + "Der Brotkasten will nicht, dass du diesen Command benutzt";

	public CDNoPermissionException()
	{
		this.notify = true;
		setMessage(this.message);
	}
	
	public CDNoPermissionException(String message)
	{
		this.notify = true;
		this.message = message;
		setMessage(message);
	}

	public CDNoPermissionException(boolean notify)
	{
		this.notify = notify;
		setMessage(this.message);
	}

	public CDNoPermissionException(boolean notify, String message)
	{
		this.notify = notify;
		this.message = message;
		setMessage(message);
	}

	@Override
	public void handle(PluginHandler handler, CommandEvent e)
	{
		handler.getCLog().log(e.getSender().getName() + " was denyed to run command '" + e.getCommand().getName() + " " + Var.arrToString(e.getArgs(), 0) + "'. The Player was " + (this.notify ? "" : "not ") + "notifyed", this);
		if (this.notify) e.getSender().sendMessage(this.message);
	}
}

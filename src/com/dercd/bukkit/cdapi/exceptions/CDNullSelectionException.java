package com.dercd.bukkit.cdapi.exceptions;

import org.bukkit.ChatColor;

import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class CDNullSelectionException extends CDSelectionException
{
	private static final long serialVersionUID = 414867063722955616L;
	public String message = Var.getExclamation(ChatColor.GOLD) + ChatColor.GOLD + "For this action you have to select a Region with WorldEdit";

	public CDNullSelectionException()
	{
	}

	public CDNullSelectionException(String message)
	{
		this.message = message;
	}

	@Override
	public void handle(PluginHandler handler, CommandEvent e)
	{
		e.getSender().sendMessage(this.message);
	}
}

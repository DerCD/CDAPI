package com.dercd.bukkit.cdapi.exceptions;

import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.events.CommandEvent;

public class CDCMessage extends CDCException
{
	private static final long serialVersionUID = -3862039995983727013L;
	public String message;

	public CDCMessage(String message)
	{
		this.message = message;
		setMessage(message);
	}

	@Override
	public void handle(PluginHandler handler, CommandEvent e)
	{
		handler.getCLog().log("Sending following message to " + e.getSender().getName() + ": " + this.message, this);
		e.getSender().sendMessage(this.message);
	}
}

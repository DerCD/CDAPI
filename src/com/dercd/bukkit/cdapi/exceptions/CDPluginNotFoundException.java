package com.dercd.bukkit.cdapi.exceptions;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class CDPluginNotFoundException extends CDCException
{
	private static final long serialVersionUID = -2587308987713529959L;
	String plugin;
	public String message;

	public CDPluginNotFoundException(String plugin)
	{
		this.plugin = plugin;
		this.message = Var.getExclamation(ChatColor.GOLD) + "The plugin " + this.plugin + " which is needed for this action is not available";
	}

	public CDPluginNotFoundException(String plugin, String message)
	{
		this.plugin = plugin;
		this.message = message;
	}

	@Override
	public void handle(PluginHandler handler, CommandEvent e)
	{
		handler.getCLog().log("Necessary Plugin '" + this.plugin + "' for action from " + e.getSender().getName() + " is not available", this);
		e.getSender().sendMessage(this.message);
	}

	public void handle(PluginHandler handler, CommandSender s)
	{
		handler.getCLog().log("Necessary Plugin '" + this.plugin + "' for action from " + s.getName() + " is not available", this);
		s.sendMessage(this.message);
	}
}

package com.dercd.bukkit.cdapi.exceptions;

import org.bukkit.ChatColor;

import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.tools.Tools.Var;
import com.dercd.bukkit.plugins.Man;

public class CDInvalidArgsException extends CDCException
{
	private static final long serialVersionUID = 2315402008574479470L;
	private String command;
	public String message = Var.getExclamation(ChatColor.GOLD) + ChatColor.GOLD + "The Plugin does not accept that count or kind of arguments";
	
	public CDInvalidArgsException()
	{
		setMessage(this.message);
	}
	
	public CDInvalidArgsException(String command)
	{
		this.command = command.toLowerCase();
		setMessage(this.message);
	}

	public CDInvalidArgsException(String command, String message)
	{
		this.command = command.toLowerCase();
		this.message = message;
		setMessage(message);
	}

	public String getCommand()
	{
		return this.command;
	}

	@Override
	public void handle(PluginHandler handler, CommandEvent e)
	{
		if(this.command == null) this.command = e.getStrCommand().toLowerCase();
		e.getSender().sendMessage(this.message);
		if(e.getSender().hasPermission("cd.man") && handler.getCommandListener().checkCommand(this.command, e.getSender()) && handler.getPlugin(Man.class).hasHelp(this.command))
		{
			e.getSender().sendMessage(Man.mbeg + "There is a help available for this command");
			e.getSender().sendMessage(Man.mbeg + "Type " + ChatColor.ITALIC + ChatColor.BOLD + "/man " + this.command + ChatColor.GRAY + " to show it");
		}
	}
}

package com.dercd.bukkit.cdapi.exceptions;

import java.lang.reflect.Field;

public abstract class CDException extends Exception
{
	private static Field messageField;
	private static final long serialVersionUID = -858267569883188266L;
	
	static
	{
		try
		{
			messageField = Throwable.class.getDeclaredField("message");
		}
		catch (Exception x) { }
	}
	
	protected void setMessage(String message)
	{
		if(messageField == null) return;
		try
		{
			messageField.set(this, message);
		}
		catch (Exception x) { }
	}
}

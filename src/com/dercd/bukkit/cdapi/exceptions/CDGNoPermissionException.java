package com.dercd.bukkit.cdapi.exceptions;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class CDGNoPermissionException extends CDGException implements Handleable
{
	private static final long serialVersionUID = -2300386686996531570L;
	private String message = Var.getExclamation(ChatColor.RED) + ChatColor.DARK_RED + "Der Brotkasten will nicht, dass du das machst!";
	private Log clog;
	
	
	public CDGNoPermissionException(Player p)
	{
		this(p, Log.getInstance());
	}
	public CDGNoPermissionException(Player p, String message)
	{
		this(p, message, Log.getInstance());
	}
	public CDGNoPermissionException(Player p, Log clog)
	{
		super(p);
		this.clog = clog;
		setMessage(this.message);
	}
	public CDGNoPermissionException(Player p, String message, Log clog)
	{
		super(p);
		this.message = message;
		this.clog = clog;
		setMessage(message);
	}

	@Override
	public void handle()
	{
		this.p.sendMessage(this.message);
		this.clog.log("GeneralException (NoPermissionException) thrown. Message: " + this.message, this);
	}
}

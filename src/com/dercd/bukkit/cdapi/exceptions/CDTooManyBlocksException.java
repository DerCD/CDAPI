package com.dercd.bukkit.cdapi.exceptions;

import org.bukkit.ChatColor;

import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class CDTooManyBlocksException extends CDSelectionException
{
	private static final long serialVersionUID = -863564129156924588L;
	public String message = Var.getExclamation(ChatColor.GOLD) + ChatColor.GOLD + "You select too many blocks";

	public CDTooManyBlocksException()
	{
	}

	public CDTooManyBlocksException(String message)
	{
		this.message = message;
	}

	public CDTooManyBlocksException(int neededBlocks)
	{
		this.message += ". You have to select " + neededBlocks + " blocks";
	}

	@Override
	public void handle(PluginHandler handler, CommandEvent e)
	{
		e.getSender().sendMessage(this.message);
	}
}

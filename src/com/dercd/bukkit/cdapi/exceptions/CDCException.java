package com.dercd.bukkit.cdapi.exceptions;

import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.events.CommandEvent;

public abstract class CDCException extends CDException
{
	private static final long serialVersionUID = 5852946734258157096L;

	@SuppressWarnings("unused")
	public void handle(PluginHandler handler, CommandEvent e) throws CDUnsupportedHandlerException, CDInvalidArgsException
	{
		throw new CDUnsupportedHandlerException("handle(PluginHandler, CommandEvent)", this.getClass().getName());
	}

	public void handle(PluginHandler handler) throws CDUnsupportedHandlerException
	{
		throw new CDUnsupportedHandlerException("handle(PluginHandler)", this.getClass().getName());
	}
}

package com.dercd.bukkit.cdapi.wrapper;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;

import com.dercd.bukkit.cdapi.CDAPI;

public class PluginMessage implements Cancellable
{
	private byte[] rawData;
	private Player p;
	private String channel;
	private boolean cancelled = false;
	private String subChannel;
	private String message;

	public PluginMessage(String channel, Player p, byte[] data)
	{
		this.channel = channel;
		this.p = p;
		this.rawData = data;
		wrapData(data);
	}

	private void wrapData(byte[] data)
	{
		try
		{
			DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
			this.subChannel = dis.readUTF();
			this.message = dis.readUTF();
		}
		catch (Exception x)
		{
			CDAPI.getInstance().getHandler().getCLog().printException(x);
		}
	}

	public String getSubChannel()
	{
		return this.subChannel;
	}

	public String getMessage()
	{
		return this.message;
	}

	public byte[] getRawData()
	{
		return this.rawData;
	}

	public Player getPlayer()
	{
		return this.p;
	}

	public String getChannel()
	{
		return this.channel;
	}

	@Override
	public boolean isCancelled()
	{
		return this.cancelled;
	}

	@Override
	public void setCancelled(boolean b)
	{
		this.cancelled = b;
	}
}

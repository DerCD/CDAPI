package com.dercd.bukkit.cdapi.tools.blockdata;

import net.minecraft.server.v1_9_R1.BlockStateBoolean;

public class BoolData extends BlockVariantData<Boolean>
{	
	public BoolData(String name, Boolean value)
	{
		super(BlockStateBoolean.of(name), value);
	}
	
	public static BoolData getNullData()
	{
		return NullData.BoolNullData.get();
	}
}

package com.dercd.bukkit.cdapi.tools.blockdata;

import net.minecraft.server.v1_9_R1.BlockStateEnum;
import net.minecraft.server.v1_9_R1.INamable;

public class EnumData<T extends Enum<T> & INamable> extends BlockVariantData<T>
{
	
	@SuppressWarnings("unchecked")
	public EnumData(String name, T value)
	{
		super(BlockStateEnum.of(name, (Class<T>) value.getClass()), value);
	}
	
	
	public static <T extends Enum<T> & INamable> EnumData<T> getNullData()
	{
		return NullData.EnumNullData.get();
	}
}
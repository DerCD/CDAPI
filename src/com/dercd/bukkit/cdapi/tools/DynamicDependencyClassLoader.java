package com.dercd.bukkit.cdapi.tools;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.plugin.Plugin;

import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.annotations.CDInternPluginDepend;
import com.dercd.bukkit.cdapi.annotations.CDPluginDepend;

public class DynamicDependencyClassLoader<C> extends DynamicClassLoader<C>
{
	public static Set<String> excluded = new HashSet<String>();

	public DynamicDependencyClassLoader(Log clog)
	{
		super(clog);
	}

	@Override
	protected List<C> initialize(Map<Class<? extends C>, File> classes, Class<?>[] constructorTypes, Object... constructorObjects)
	{
		checkDoDependencys(classes);
		return super.initialize(classes, constructorTypes, constructorObjects);
	}

	public void checkDoDependencys(Map<Class<? extends C>, File> classes)
	{
		this.clog.log("Checking dependencys", this);
		boolean found = true;
		while (found)
		{
			found = false;
			for (Class<? extends C> c : new HashSet<Class<? extends C>>(classes.keySet()))
				if (!checkDoDependencys(c) || !checkDoCDDependencys(c))
				{
					classes.remove(c);
					excluded.add(c.getName());
					found = true;
					break;
				}
		}
		this.clog.log("All dependencys checked", this);
	}

	public boolean checkDoDependencys(Class<?> c)
	{
		CDPluginDepend depend;
		try
		{
			if ((depend = c.getAnnotation(CDPluginDepend.class)) != null) for (Class<? extends Plugin> pluginClass : depend.depends())
				pluginClass.getName();
			return true;
		}
		catch (ArrayStoreException | NoClassDefFoundError x)
		{
			this.clog.log("Jumping " + c.getName() + "; a dependency wasn't found", this);
			this.found.put(c.getName(), true);
			return false;
		}
	}

	public boolean checkDoCDDependencys(Class<?> c)
	{
		CDInternPluginDepend depend;
		try
		{
			if ((depend = c.getAnnotation(CDInternPluginDepend.class)) != null) for (Class<? extends CDPlugin> pluginClass : depend.depends())
			{
				pluginClass.getName();
				if (excluded.contains(pluginClass.getName())) throw new NoClassDefFoundError();
			}
			return true;
		}
		catch (ArrayStoreException | NoClassDefFoundError x)
		{
			this.clog.log("Jumping " + c.getName() + "; a CD-dependency wasn't found", this);
			this.found.put(c.getName(), true);
			return false;
		}
	}
}

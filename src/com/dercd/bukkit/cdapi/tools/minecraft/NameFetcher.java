package com.dercd.bukkit.cdapi.tools.minecraft;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.exceptions.CDPlayerResolveException;
import com.dercd.bukkit.cdapi.exceptions.CDPlayerUUIDNotFoundException;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.Tools.Minecraft;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;

public class NameFetcher
{
	private Log clog = Log.getInstance();
	private static final String PROFILE_URL = "https://sessionserver.mojang.com/session/minecraft/profile/";
	private JsonParser jsonParser = new JsonParser();
	protected Map<UUID, String> fetched = new HashMap<UUID, String>();
	private UUIDFetcher uuidFetcher;
	
	public NameFetcher()
	{
		init();
	}
	
	public void init()
	{
		for(Player p : Minecraft.getOnlinePlayers())
			this.fetched.put(p.getUniqueId(), p.getName());
	}
		
	public void setUUIDFetcher(UUIDFetcher f)
	{
		this.uuidFetcher = f;
	}

	public Map<UUID, String> fetch(List<UUID> uuids) throws CDPlayerResolveException
	{
		return fetch(uuids, 0);
	}
	
	public Map<UUID, String> fetch(List<UUID> uuids, int attempt) throws CDPlayerResolveException
	{
		if(uuids == null) return ImmutableMap.copyOf(this.fetched);
		for (UUID uuid : ImmutableList.copyOf(uuids))
			if (!this.fetched.containsKey(uuid)) this.fetched.put(uuid, "###");
			else if (this.fetched.get(uuid) == null) this.fetched.put(uuid, "###");
			else if (!this.fetched.get(uuid).equals("###")) uuids.remove(uuid);
		for (UUID uuid : uuids)
		{
			try
			{
				HttpURLConnection connection = (HttpURLConnection) new URL(PROFILE_URL + uuid.toString().replace("-", "")).openConnection();
				connection.setReadTimeout(5000);
				connection.setConnectTimeout(5000);
				JsonObject response = (JsonObject) this.jsonParser.parse(new InputStreamReader(connection.getInputStream()));
				String name = response.get("name").getAsString();
				if (name == null) continue;
				if (response.has("cause"))
				{
					String cause = response.get("cause").getAsString();
					String errorMessage = response.get("errorMessage").getAsString();
					if (cause != null && cause.length() > 0)
						throw new IllegalStateException(errorMessage);
				}
				this.fetched.put(uuid, name);
				this.uuidFetcher.fetched.put(name, uuid);
				attempt = 0;
			}
			catch (Exception x)
			{
				attempt++;
				this.clog.log("Error while fetching names; " + attempt + " attempt", this);
				if (attempt >= 5)
				{
					this.clog.log("Do not try to fetch name anymore", this);
					CDPlayerResolveException toThrow = new CDPlayerResolveException();
					toThrow.initCause(x);
					throw toThrow;
				}
				return fetch(uuids, attempt);
			}
		}
		return ImmutableMap.copyOf(this.fetched);
	}

	public String fetch(UUID uuid) throws CDPlayerResolveException
	{
		try
		{
			String name = fetch(new ArrayList<UUID>(Arrays.asList(uuid))).get(uuid);
			if(name != null)
				return name;
			throw new CDPlayerUUIDNotFoundException(uuid);
		}
		catch (Exception x)
		{
			if (x instanceof CDPlayerResolveException) throw x;
			this.clog.printException(x);
			throw new CDPlayerResolveException(uuid);
		}
	}

	public void put(UUID u, String name)
	{
		this.fetched.put(u, name);
		this.uuidFetcher.fetched.put(name, u);
	}

	public static NameFetcher getInstance()
	{
		return CDAPI.getInstance().getHandler().getNameFetcher();
	}
}
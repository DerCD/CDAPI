package com.dercd.bukkit.cdapi.tools.minecraft;

import net.minecraft.server.v1_9_R1.Block;
import net.minecraft.server.v1_9_R1.IBlockData;
import net.minecraft.server.v1_9_R1.ItemStack;
import net.minecraft.server.v1_9_R1.NBTTagCompound;
import net.minecraft.server.v1_9_R1.TileEntity;
import net.minecraft.server.v1_9_R1.World;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_9_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_9_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;

import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.Tools.Data;
import com.dercd.bukkit.cdapi.tools.Tools.Minecraft;
import com.dercd.bukkit.cdapi.tools.Tools.Var;
import com.dercd.bukkit.cdapi.tools.Tools.Var.NBTType;

public class CDBlock
{
	private NBTTagCompound te;
	private IBlockData blockData;
	private Block legacyBlock;
	private byte legacyData;
	private Location l;
	
	private CDBlock(IBlockData blockData, TileEntity te, Location l)
	{
		if(te != null)
		{
			this.te = new NBTTagCompound();
			te.save(this.te);
		}
		if(l != null)
			this.l = l.clone();
		this.blockData = blockData;
		this.legacyBlock = blockData.getBlock();
		this.legacyData = (byte) this.legacyBlock.toLegacyData(blockData);
	}
	private CDBlock(IBlockData blockData, NBTTagCompound te, Location l)
	{
		this.te = (NBTTagCompound) te.clone();
		if(l != null)
			this.l = l.clone();
		this.blockData = blockData;
		this.legacyBlock = blockData.getBlock();
		this.legacyData = (byte) this.legacyBlock.toLegacyData(blockData);
	}
	private CDBlock(Block b, byte data, TileEntity te, Location l)
	{
		if(te != null)
		{
			this.te = new NBTTagCompound();
			te.save(this.te);
		}
		if(l != null)
			this.l = l.clone();
		this.legacyBlock = b;
		this.legacyData = data;
		this.blockData = b.fromLegacyData(data);
	}
	private CDBlock(Block b, byte data, NBTTagCompound te, Location l)
	{
		this.te = (NBTTagCompound) te.clone();
		if(l != null)
			this.l = l.clone();
		this.legacyBlock = b;
		this.legacyData = data;
		this.blockData = b.fromLegacyData(data);
	}
	
	public void setLocation(Location l)
	{
		this.l = l.clone();
	}
	
	public void place(boolean updateBlock)
	{
		place(this.l, updateBlock);
	}
	public void place()
	{
		place(this.l, true);
	}
	public void place(Location l)
	{
		place(l, true);
	}
	public void place(Location l, boolean updateBlock)
	{
		if(l == null) return;
		World w = ((CraftWorld) l.getWorld()).getHandle();
		if(updateBlock) Minecraft.setBlockWithUpdate(l.getBlockX(), l.getBlockY(), l.getBlockZ(), this.blockData, w);
		else Minecraft.setBlockWithoutUpdate(l.getBlockX(), l.getBlockY(), l.getBlockZ(), this.blockData, w);
		if(this.te == null) return;
		TileEntity blockTE = w.getTileEntity(Tools.Minecraft.getBlockPosition(l));
		if(blockTE == null) return;
		NBTTagCompound teToSet = (NBTTagCompound) this.te.clone();
		teToSet.setInt("x", l.getBlockX());
		teToSet.setInt("y", l.getBlockY());
		teToSet.setInt("z", l.getBlockZ());
		blockTE.a(teToSet);
	}
	public void placeForPlayer(Player p)
	{
		placeForPlayer(this.l, p);
	}
	public void placeForPlayer(Location l, Player p)
	{
		if(l == null)
			return;
		Data.sendBlockChange(p, l, this.blockData);
	}
	
	@Deprecated
	public Block getBlock()
	{
		return this.legacyBlock;
	}
	@Deprecated
	public byte getData()
	{
		return this.legacyData;
	}
	public TileEntity getTileEntity()
	{
		return TileEntity.a(Minecraft.mcServer, this.te);
	}
	public NBTTagCompound getTileEntityAsCompound()
	{
		return this.te != null ? (NBTTagCompound) this.te.clone() : null;
	}
	public IBlockData getBlockData()
	{
		return this.blockData;
	}
	public Location getLocation()
	{
		return this.l != null ? this.l.clone() : null;
	}
	
	public NBTTagCompound toNBT()
	{
		NBTTagCompound cpd = new NBTTagCompound();
		if(this.l != null)
			Var.writeCoords(cpd, this.l);
		cpd.setByte("tileId", (byte) this.blockData.getBlock().toLegacyData(this.blockData));
		cpd.setString("id", Block.REGISTRY.b(this.legacyBlock).a());
		if(this.te != null)
			cpd.set("tileEntity", this.te);
		return cpd;
	}
	
	//TODO: Create item get when Ctrl+Item-Picker clicked
	//e. g. Create item from CommandBlock with command saved in the item
	public ItemStack toItem()
	{
		ItemStack i = new ItemStack(this.legacyBlock);
		i.setData(this.legacyData);
		return i;
	}
	public org.bukkit.inventory.ItemStack toBukkitItem()
	{
		return CraftItemStack.asBukkitCopy(toItem());
	}
	
	@Override
	public CDBlock clone()
	{
		return new CDBlock(this.blockData, this.te == null ? null : (NBTTagCompound) this.te.clone(), this.l);
	}
	@Override
	public int hashCode()
	{
		int hash = 42;
		hash *= (this.te == null ? 13 : this.te.hashCode());
		hash += 42;
		hash *= (this.legacyBlock == null ? 27 : this.legacyBlock.hashCode());
		hash -= 42;
		hash *= this.legacyData;
		hash += 42;
		hash *= (this.l == null ? 41 : this.l.hashCode());
		hash -= 42;
		return hash;
	}
	@Override
	public boolean equals(Object o)
	{
		if(o == this)
			return true;
		
		if(o == null || !(o instanceof CDBlock))
			return false;
		
		CDBlock cdb = (CDBlock) o;
		return
				cdb.legacyBlock.equals(this.legacyBlock) &&
				cdb.legacyData == this.legacyData &&
				(this.te == null ? cdb.te == null : this.te.equals(cdb.te)) &&
				(this.l == null ? cdb.l == null : this.l.equals(cdb.l));
	}
	
	public static CDBlock fromBlock(org.bukkit.block.Block b)
	{
		Location l = b.getLocation();
		World w = ((CraftWorld) b.getWorld()).getHandle();
		IBlockData blockData = w.getType(Minecraft.getBlockPosition(l));
		TileEntity te = w.getTileEntity(Tools.Minecraft.getBlockPosition(l));
		return new CDBlock(blockData, te, l);
	}
	public static CDBlock fromBlock(Location l)
	{
		return fromBlock(l.getBlock());
	}
	public static CDBlock fromNBT(NBTTagCompound cpd)
	{
		Location l = Var.readCoords(cpd);
		byte data = cpd.getByte("tileId");
		Block b;
		if(Var.hasKey(cpd, "id", NBTType.INT))
			b = Block.getById(cpd.getInt("id"));
		else
			b = Block.getByName(cpd.getString("id"));
		TileEntity te = null;
		if(cpd.hasKeyOfType("tileEntity", 10))
			te = TileEntity.a(Minecraft.mcServer, cpd.getCompound("tileEntity"));
		return new CDBlock(b, data, te, l);
	}
	
	@Deprecated
	public static CDBlock fromMaterial(Material m, byte tileId)
	{
		return new CDBlock(Block.getById(m.getId()).fromLegacyData(tileId), (TileEntity) null, null);
	}
	@SuppressWarnings("deprecation")
	public static CDBlock fromMaterial(Material m)
	{
		return new CDBlock(Block.getById(m.getId()).getBlockData(), (TileEntity) null, null);
	}
	public static CDBlock fromMaterial(Block b)
	{
		return fromMaterial(b, (byte) 0);
	}
	@Deprecated
	public static CDBlock fromMaterial(Block b, byte tileId)
	{
		return new CDBlock(b.fromLegacyData(tileId), (TileEntity) null, null);
	}
}

package com.dercd.bukkit.cdapi.tools.minecraft;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import net.minecraft.server.v1_9_R1.PacketPlayOutTitle.EnumTitleAction;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.Tools.Minecraft;
import com.dercd.bukkit.cdapi.tools.collection.PresetMap;

public class Countdown
{
	private int time;
	private BukkitTask task;
	private Map<Integer, List<Runnable>> runnables = new PresetMap<Integer, List<Runnable>>(() -> { return new ArrayList<Runnable>(); });
	List<Runnable> pending = new ArrayList<Runnable>();
	
	protected boolean first = true,
						paused = false,
						wasPaused = false;
	
	protected int fullTime;
	private boolean 	running = false,
						async = false,
						asyncChanged = false,
						startNow = true;
	protected PluginHandler handler;
	
	protected Runnable messenger = () ->
	{
		if(this.paused || !this.running || !updateAsync()) return;
		if(this.wasPaused)
		{
			runPending();
			if(this.paused) return;
			messageTime();
			this.wasPaused = false;
			if(this.time <= 0) cancel();
			return;
		}
		if(!this.first)
			updateTime();
		checkTime();
		if(!this.running || this.paused) return;
		messageTime();
		if (this.time <= 0)
			cancel();
		Countdown.this.first = false;
	}, r;
	
	public static SimpleDateFormat hourSDF, minuteSDF, hourMinuteOnlySDF;
	
	
	List<Display> displays = new ArrayList<Display>();
	
	static Log clog;
	
	static
	{
		hourSDF = new SimpleDateFormat("HH:mm:ss");
		hourSDF.setTimeZone(TimeZone.getTimeZone("UTC"));
		minuteSDF = new SimpleDateFormat("mm:ss");
		minuteSDF.setTimeZone(TimeZone.getTimeZone("UTC"));
		hourMinuteOnlySDF = new SimpleDateFormat("HH:mm");
		hourMinuteOnlySDF.setTimeZone(TimeZone.getTimeZone("UTC"));
	}
	
	
	public Countdown(PluginHandler handler, int time, Option... options)
	{
		if(clog == null) clog = handler.getCLog();
		this.handler = handler;
		this.time = time;
		this.fullTime = this.time;
		for(Option o : options)
			addOption(o);
		generateMessages();
		if(this.startNow) start();
	}
	
	
	public void addOption(Option o)
	{
		addOption(o.type, o.obj);
	}
	public void addOption(String type, Object o)
	{
		switch(type)
		{
			case "async":
				this.async = (boolean) o;
				break;
			case "start_now":
				this.startNow = (boolean) o;
				break;
			case "display":
				this.displays.add((Display) o);
				break;
			case "runnable":
				Object[] oarr = (Object[]) o;
				if((int) oarr[1] < 0) return;
				this.runnables.get(oarr[1]).add((Runnable) oarr[0]);
				break;
			default:
				return;
		}
	}
	

	public synchronized void updateTime()
	{
		--this.time;
	}
	
	public void generateMessages()
	{
		for(Display d : this.displays)
			d.generateMessages();
	}
	
	public void messageTime()
	{
		
		for(Display d : this.displays)
			d.display(this.time);
	}
	protected synchronized boolean updateAsync()
	{
		if(this.asyncChanged)
		{
			this.asyncChanged = false;
			newThread();
			return false;
		}
		return true;
	}

	public boolean checkTime()
	{
		if(this.runnables.containsKey(this.time))
			for(Runnable r : this.runnables.get(this.time))
				if(this.paused)
					this.pending.add(r);
				else
					r.run();
		return true;
	}
	public void runPending()
	{
		Iterator<Runnable> i = this.pending.iterator();
		while(i.hasNext() && !this.paused)
		{
			i.next().run();
			i.remove();
		}
	}
	
	public void cancel()
	{
		if (this.task != null) this.task.cancel();
		this.running = false;
	}

	public void setTime(int i)
	{
		this.time = i;
	}
	public void setTime(int i, boolean pause)
	{
		if(pause)
		{
			this.first = true;
			this.asyncChanged = true;
		}
		this.time = i;
	}

	public int getTime()
	{
		return this.time;
	}
	
	public List<Display> getDisplays()
	{
		return this.displays;
	}
	
	public boolean isRunning()
	{
		return this.running;
	}
	public boolean isPaused()
	{
		return this.paused;
	}
	
	public void pause()
	{
		this.paused = true;
	}
	public void unpause()
	{
		if(!this.paused) return;
		this.wasPaused = true;
		this.paused = false;
	}
	
	
	public void start()
	{
		if(this.running) return;
		newThread();
		this.running = true;
	}
	
	public boolean isAsync()
	{
		return this.async;
	}
	public synchronized void setAsync(boolean async)
	{
		this.async = async;
		this.asyncChanged = !this.asyncChanged;
	}
	
	private synchronized void newThread()
	{
		if(this.task != null)
			this.task.cancel();
		if (this.async) this.task = Bukkit.getScheduler().runTaskTimerAsynchronously(this.handler.getMain(), this.messenger, 0, 20);
		else this.task = Bukkit.getScheduler().runTaskTimer(this.handler.getMain(), this.messenger, 0, 20);
	}
	
	
	
	public static enum OptionType
	{
		DISPLAY("display"),
		ASYNC("async"),
		START_NOW("start_now"),
		RUNNABLE("runnable");
		
		private String s;
		private OptionType(String s)
		{
			this.s = s;
		}
		
		@Override
		public String toString()
		{
			return this.s;
		}
	}
	
	public static class Option
	{
		public String type;
		public Object obj;
		
		public Option(String type, Object obj)
		{
			this.type = type;
			this.obj = obj;
		}
		public Option(OptionType type, Object obj)
		{
			this.type = type.toString();
			this.obj = obj;
		}
		
	}
	public static class RunnableOption extends Option
	{
		public RunnableOption(Runnable r)
		{
			this(r, 0);
		}
		public RunnableOption(Runnable r, int time)
		{
			super(OptionType.RUNNABLE, new Object[] { r, time });
		}
	}
	
	public static abstract class Display
	{
		public Map<Integer, String> messages;
		protected String format;
		protected Collection<? extends Player> affected, realAffected;
		protected Set<Integer> times = new HashSet<Integer>();
		protected boolean updatePlayers = true;
		boolean allPlayers = false;
		
		public Display(String format, Iterable<Integer> times, Collection<? extends Player> affected, boolean updatePlayers)
		{
			if(clog == null) clog = CDAPI.getInstance().getHandler().getCLog();
			this.format = format;
			if(times != null)
				for(Integer i : times)
					if(i != null)
						this.times.add(i);
			this.realAffected = affected;
			if(affected == null)
				this.allPlayers = true;
			else
				this.affected = new HashSet<Player>(affected);
			this.updatePlayers = updatePlayers;
			if(this.updatePlayers)
				updatePlayers();
		}
		
		
		public void display(int time)
		{
			if(!this.times.contains(time)) return;
			if(this.updatePlayers) updatePlayers();
			if(this.messages == null) display(null);
			else display(this.messages.get(time));
		}
		
		public abstract void display(String text);
		
		public void setAffectedPlayers(Collection<? extends Player> affected)
		{
			this.realAffected = affected;
			this.affected = affected;
			if(this.updatePlayers)
				updatePlayers();
			this.allPlayers = affected == null;
		}
		
		public Collection<? extends Player> getAffectedPlayers()
		{
			return this.affected;
		}
		
		public Collection<? extends Player> getRealAffectedPlayer() 
		{
			return this.realAffected;
		}
		
		public boolean allUsersAffected()
		{
			return this.allPlayers;
		}
		
		public boolean getUpdatePlayers()
		{
			return this.updatePlayers;
		}
		
		public void setUpdatePlayers(boolean updatePlayers)
		{
			this.updatePlayers = updatePlayers;
		}
		
		public void updatePlayers()
		{
			
			if(this.allPlayers)
				this.affected = new HashSet<Player>(Minecraft.getOnlinePlayers());
			else
			{
				this.affected = new HashSet<Player>(this.realAffected);
				this.affected.retainAll(Minecraft.getOnlinePlayers());
			}
		}
	
	
		public void generateMessages()
		{
			if(this.times == null) return;
			if(this.format == null) return;
			this.messages = new HashMap<Integer, String>();
			for(Integer time : this.times)
				if(time != null)
					this.messages.put(time, generateMessage(this.format, time));
		}
		private String generateMessage(String format, int time)
		{
			String[] s = generateMessage(time);
			return format.replace("<time1>", s[0]).replace("<time2>", s[1]).replace("<time_seconds>", "" + time).replace("<time_minutes>", "" + (time / 60)).replace("<time_hours>", "" + (time / 3600));
		}
		private String[] generateMessage(int time)
		{
			if(time <= 60) return new String[] { "" + time, "second" + (time == 1 ? "" : "s") };
			if(time > 3600)
				if(time % 3600 == 0)
					return new String[] { "" + time / 3600, "hours" };
				else if(time % 60 == 0)
					return new String[] { hourMinuteOnlySDF.format(new Date(time * 1000)), "hours" };
				else return new String[] { hourSDF.format(new Date(time * 1000)), "hours" };
			if(time == 3600) return new String[] { "60", "minutes" };
			if(time % 60 == 0)
				return new String[] { "" + time / 60, "minutes" };
			return new String[] { minuteSDF.format(new Date(time * 1000)), "minutes" };
		}
	
	}

	
	public static class ChatDisplay extends Display
	{
		
		public ChatDisplay(String format, Iterable<Integer> times, Collection<? extends Player> affected)
		{
			this(format, times, affected, true);
		}
		
		public ChatDisplay(String format, Iterable<Integer> times, Collection<? extends Player> affected, boolean updatePlayers)
		{
			super(format, times, affected, updatePlayers);
		}
		
		@Override
		public void display(String text)
		{
			if(this.allPlayers)
				Bukkit.broadcastMessage(text);
			else
				for(Player p : this.affected)
					if(p.isOnline())
						p.sendMessage(text);
		}
	}
	
	public static class XPDisplay extends Display
	{
		
		int fullTime;
		
		public XPDisplay(Collection<? extends Player> affected, int fullTime)
		{
			this(affected, fullTime, true);
		}
		
		public XPDisplay(Collection<? extends Player> affected, int fullTime, boolean updatePlayers)
		{
			super(null, null, affected, updatePlayers);
			this.fullTime = fullTime;
		}
		
		@Override
		public void display(int time)
		{
			if(this.updatePlayers) updatePlayers();
			float xp;
			if (this.fullTime == 0) xp = 0;
			else xp = ((float) time) / this.fullTime;
			if(this.allPlayers)
				Minecraft.setLevelAndExp(time, xp);
			else
				for(Player p : this.affected)
					if(p.isOnline())
						Minecraft.setLevelAndExp(time, xp, p);
		}
		
		
		@Override
		public void display(String text)
		{
		}
		
		@Override
		public void generateMessages()
		{
		}
	}
	
	public static class TitleDisplay extends Display
	{
		EnumTitleAction a;
		int[] times;
		String subTitle;
		
		
		public TitleDisplay(String title, String subTitle, Iterable<Integer> times, Collection<? extends Player> affected, int[] displayTimes)
		{
			this(title, subTitle, times, affected, true, displayTimes);
		}
		public TitleDisplay(String title, String subTitle, Iterable<Integer> times, Collection<? extends Player> affected, boolean updatePlayers, int[] displayTimes)
		{
			this(title, times, affected, updatePlayers, EnumTitleAction.TITLE, displayTimes);
			this.subTitle = subTitle;
		}
		
		public TitleDisplay(String format, Iterable<Integer> times, Collection<? extends Player> affected, EnumTitleAction a, int[] displayTimes)
		{
			this(format, times, affected, true, a, displayTimes);
		}
		public TitleDisplay(String format, Iterable<Integer> times, Collection<? extends Player> affected, boolean updatePlayers, EnumTitleAction a, int[] displayTimes)
		{
			super(format, times, affected, updatePlayers);
			this.a = a;
			this.times = displayTimes;
		}
		
		@Override
		public void display(String text)
		{
			if(this.subTitle != null)
				Minecraft.sendTitle(text, this.subTitle, this.affected, this.times, true);
			else
				Minecraft.sendTitle(text, this.a, this.affected, this.times, true);
		}
	}

	public static class SoundDisplay extends Display
	{
		private Sound s;
		
		public SoundDisplay(Iterable<Integer> times, Collection<? extends Player> affected, Sound s)
		{
			this(times, affected, true, s);
		}
		
		public SoundDisplay(Iterable<Integer> times, Collection<? extends Player> affected, boolean updatePlayers, Sound s)
		{
			super(null, times, affected, updatePlayers);
			this.s = s;
		}
		
		
		public Sound getSound()
		{
			return this.s;
		}
		
		public void setSound(Sound s)
		{
			this.s = s;
		}

		@Override
		public void display(String text)
		{
			Minecraft.playSound(this.affected, this.s);
		}
	}
}

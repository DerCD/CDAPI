package com.dercd.bukkit.cdapi.tools.collection;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class SyncedSetMap<K, V>
{
	protected Map<K, Set<V>> map1 = new HashMap<K, Set<V>>();
	protected Map<V, Set<K>> map2 = new HashMap<V, Set<K>>();
	
	@SuppressWarnings("unchecked")
	public void add(K key, V value)
	{
		Set<V> set1 = this.map1.get(key);
		if(set1 == null)
			this.map1.put(key, (set1 = new SyncedSetMapSet<V, K, V>(this, key, null)));
		Set<K> set2 = this.map2.get(value);
		if(set2 == null)
			this.map2.put(value, (set2 = new SyncedSetMapSet<K, K, V>(this, null, value)));
		((SyncedSetMapSet<V, K, V>) set1).unsafeAdd(value);
		((SyncedSetMapSet<K, K, V>) set2).unsafeAdd(key);
	}
	
	public Set<V> getByKey(K key)
	{
		Set<V> back = this.map1.get(key);
		if(back == null)
			this.map1.put(key, (back = new SyncedSetMapSet<V, K, V>(this, key, null)));
		return back;
	}
	public Set<K> getByVal(V val)
	{
		Set<K> back = this.map2.get(val);
		if(back == null)
			this.map2.put(val, (back = new SyncedSetMapSet<K, K, V>(this, null, val)));
		return back;
	}
	
	@SuppressWarnings("unchecked")
	public Set<V> removeKey(K key)
	{
		Set<V> removed = this.map1.remove(key);
		if(removed == null) return null;
		for(V val : removed)
			((SyncedSetMapSet<K, K, V>) this.map2.get(val)).unsafeRemove(key);
		return removed;
	}
	@SuppressWarnings("unchecked")
	public Set<K> removeVal(V val)
	{
		Set<K> removed = this.map2.remove(val);
		if(removed == null) return null;
		for(K key : removed)
			((SyncedSetMapSet<V, K, V>) this.map1.get(key)).unsafeRemove(val);
		return removed;
	}
	
	@SuppressWarnings("unchecked")
	public boolean removeByKey(V val, K key)
	{
		SyncedSetMapSet<V, K, V> set1 = (SyncedSetMapSet<V, K, V>) this.map1.get(key);
		if(set1 == null)
		{
			this.map1.put(key, (set1 = new SyncedSetMapSet<V, K, V>(this, key, null)));
			return false;
		}
		SyncedSetMapSet<K, K, V> set2 = (SyncedSetMapSet<K, K, V>) this.map2.get(val);
		if(set2 == null)
		{
			this.map2.put(val, (set2 = new SyncedSetMapSet<K, K, V>(this, null, val)));
			return false;
		}
		set2.unsafeRemove(key);
		return set1.unsafeRemove(val);
	}
	
	public boolean containsKey(K key)
	{
		return this.map1.containsKey(key) && this.map1.get(key).size() != 0;
	}
	public boolean containsVal(V val)
	{
		return this.map2.containsKey(val) && this.map2.get(val).size() != 0;
	}
	
	public Set<K> keySet()
	{
		return new HashSet<K>(this.map1.keySet());
	}
	public Set<V> keySetOther()
	{
		return new HashSet<V>(this.map2.keySet());
	}
	
	public void clear()
	{
		this.map1.clear();
		this.map2.clear();
	}
	public void clearByKey(K key)
	{
		removeKey(key);
		getByKey(key);
	}
	public void clearByVal(V val)
	{
		removeVal(val);
		getByVal(val);
	}
	
	public Map<K, Set<V>> getMap1()
	{
		Map<K, Set<V>> back = new HashMap<K, Set<V>>(this.map1);
		for(Entry<K, Set<V>> e : back.entrySet())
			e.setValue(new HashSet<V>(e.getValue()));
		return back;
	}
	public Map<V, Set<K>> getMap2()
	{
		Map<V, Set<K>> back = new HashMap<V, Set<K>>(this.map2);
		for(Entry<V, Set<K>> e : back.entrySet())
			e.setValue(new HashSet<K>(e.getValue()));
		return back;
	}
}

class SyncedSetMapSet<C, K, V> extends HashSet<C>
{
	private static final long serialVersionUID = 2369730382023901257L;
	final SyncedSetMap<K, V> parent;
	final boolean isPrimaryMap;
	final K k;
	final V v;
	
	public SyncedSetMapSet(SyncedSetMap<K, V> parent, K k, V v)
	{
		this.parent = parent;
		this.k = k;
		this.v = v;
		this.isPrimaryMap = v == null;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public boolean add(C val)
	{
		boolean back = !contains(val);
		if(this.isPrimaryMap)
			this.parent.add(this.k, (V) val);
		else
			this.parent.add((K) val, this.v);
		return back;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public boolean remove(Object val)
	{
		if(this.isPrimaryMap)
			return this.parent.removeByKey((V) val, this.k);
		return this.parent.removeByKey(this.v, (K) val);
	}
	
	@Override
	public void clear()
	{
		for(C val : this)
			remove(val);
	}
	
	@Override
	public Iterator<C> iterator()
	{
		Set<C> back = new HashSet<C>();
		Iterator<C> i = super.iterator();
		while(i.hasNext())
			back.add(i.next());
		return back.iterator();
	}
	
	
	protected boolean unsafeAdd(C val)
	{
		return super.add(val);
	}
	protected boolean unsafeRemove(Object val)
	{
		return super.remove(val);
	}
	protected void unsafeClear()
	{
		super.clear();
	}
}

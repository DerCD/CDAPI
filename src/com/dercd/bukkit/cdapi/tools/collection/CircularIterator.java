package com.dercd.bukkit.cdapi.tools.collection;

import java.util.Iterator;

public class CircularIterator<C> implements Iterator<C>
{
	Iterator<C> real;
	Iterable<C> i;

	
	public CircularIterator(Iterable<C> i)
	{
		this.real = i.iterator();
		this.i = i;
		if(!this.real.hasNext()) throw new IllegalArgumentException();
	}
	

	@Override
	public boolean hasNext()
	{
		return true;
	}

	@Override
	public C next()
	{
		if(!this.real.hasNext())
			this.real = this.i.iterator();
		return this.real.next();
	}
}

package com.dercd.bukkit.cdapi.tools;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class DynamicClassLoader<C>
{
	public static CDClassLoader createdClassLoader;
	public static Map<URL, ClassLoader> preCreatedClassLoaders = new HashMap<URL, ClassLoader>();
	Log clog;
	Map<Class<? extends C>, File> files = new HashMap<Class<? extends C>, File>();
	public Map<String, Boolean> found = new HashMap<String, Boolean>();
	
	public DynamicClassLoader()
	{
		this(Log.getInstance());
	}
	public DynamicClassLoader(Log clog)
	{
		this.clog = clog;
	}

	public List<C> loadDir(File dir, Class<C> clazz, Class<?>[] constructorTypes, Object... constructorObjects)
	{
		return initialize(searchDir(dir, clazz, constructorTypes, constructorObjects), constructorTypes, constructorObjects);
	}

	private Map<Class<? extends C>, File> searchDir(File dir, Class<C> clazz, Class<?>[] constructorTypes, Object... constructorObjects)
	{
		String dirName = dir.getAbsolutePath();
		dirName = Tools.Var.removeUselessPath(dirName);
		dirName = Tools.Var.addEndingToAbsulutePath(dirName);
		this.clog.log("Searching directory " + dir.getName(), this);
		Map<Class<? extends C>, File> map = new HashMap<Class<? extends C>, File>();
		if (dir == null || !dir.exists() || !dir.isDirectory()) return map;
		this.files.clear();
		File file;
		//Code without lambda
//		for (String f : dir.list(new FilenameFilter()
//		{
//			@Override
//			public boolean accept(File arg0, String arg1)
//			{
//				return arg1.endsWith(".jar") || arg1.endsWith(".zip");
//			}
//		}))
		//Code with lambda
		String[] files = dir.list((folder, name) -> name.endsWith(".jar") || name.endsWith(".zip"));
		for(String f : files) load(new File(dirName + f));
		for(String f : files) map.putAll(Tools.Var.toMap(search((file = new File(dirName + f)), clazz), file));
		return map;
	}

	public List<C> loadFiles(File[] files, Class<C> clazz, Class<?>[] constructorTypes, Object... constructorObjects)
	{
		Map<Class<? extends C>, File> map = new HashMap<Class<? extends C>, File>();
		for (File f : files)
			if (f.isDirectory()) map.putAll(searchDir(f, clazz, constructorTypes, constructorObjects));
			else
			{
				load(f);
				map.putAll(Tools.Var.toMap(search(f, clazz), f));
			}
		return initialize(map, constructorTypes, constructorObjects);
	}
	public List<C> loadFile(File f, Class<C> clazz, Class<?>[] constructorTypes, Object... constructorObjects)
	{
		return initialize(Tools.Var.toMap(search(f, clazz), f), constructorTypes, constructorObjects);
	}
	
	protected void load(File f)
	{
		try
		{
			this.clog.log("Loading file " + f.getAbsolutePath(), this);
			URL url = f.toURI().toURL();
			if (!preCreatedClassLoaders.containsKey(url))
				if (createdClassLoader == null) createdClassLoader = new CDClassLoader(new URL[] { url }, getClass().getClassLoader());
				else createdClassLoader.addURL(url);
		}
		catch (Exception x)
		{
			this.clog.log("Error while loading file " + f.getName(), this);
			this.clog.printException(x);
		}
	}
	protected List<Class<? extends C>> search(File f, Class<C> clazz)
	{
		this.clog.log("Searching file " + f.getAbsolutePath(), this);
		List<Class<? extends C>> classes = new ArrayList<Class<? extends C>>();
		try
		{
			ZipFile z = new ZipFile(f);
			Enumeration<? extends ZipEntry> en = z.entries();
			URL url = f.toURI().toURL();
			ClassLoader cloader = preCreatedClassLoaders.containsKey(url) ? preCreatedClassLoaders.get(url) : createdClassLoader;
			Class<?> c;
			Class<? extends C> cdc;
			String name = "";
			int classCounter = 0;
			while (en.hasMoreElements())
			{
				try
				{
					this.clog.debug("Searching class number " + ++classCounter, this);
					name = en.nextElement().getName();
					this.clog.debug("Name pre: " + name, this);
					name = name.substring(0, name.length() - 6).replace('/', '.');
					this.clog.debug("Name post: " + name, this);
					if (!checkDoFound(name)) continue;
					this.clog.debug("Name found: " + name, this);
					c = Class.forName(name, true, cloader);
					if (!clazz.isAssignableFrom(c)) continue;
					this.clog.debug("Class found: " + name, this);
					cdc = c.asSubclass(clazz);
					this.clog.log("Found class " + (name.contains(".") ? name.substring(name.lastIndexOf('.') + 1) : name) + ": " + name, this);
					this.found.put(name, true);
					classes.add(cdc);
				}
				catch (Throwable ix)
				{
					if(this.clog.isDebug(this))
						this.clog.printException(ix);
					DynamicDependencyClassLoader.excluded.add(name);
					continue;
				}
			}
			z.close();
		}
		catch (Exception x)
		{
			this.clog.log("Error while searching file " + f.getName(), this);
			this.clog.printException(x);
			return null;
		}
		return classes;
	}

	@SuppressWarnings("unchecked")
	protected List<C> initialize(Map<Class<? extends C>, File> classes, Class<?>[] constructorTypes, Object... constructorObjects)
	{
		this.clog.log("Initializing objects", this);
		List<C> back = new ArrayList<C>();
		boolean success;
		for (Class<? extends C> c : classes.keySet())
			try
			{
				if (c == null) continue;
				if(Modifier.isAbstract(c.getModifiers()))
				{
					this.clog.log("Don't initialize abstract class " + c.getName(), this);
					continue;
				}
				success = false;
				for (Constructor<?> con : c.getConstructors())
					if (Arrays.equals(con.getParameterTypes(), constructorTypes))
					{
						back.add((C) con.newInstance(constructorObjects));
						success = true;
						break;
					}
				if(!success)
					this.clog.log("No appropriate constructor in class " + c.getName() + " found. Couln't initialize", this);
				else
					this.files.put(c, classes.get(c));
			}
			catch (Exception x)
			{
				this.clog.log("Error while initializing class " + c.getName(), this);
			}
		return back;
	}

	protected boolean checkDoFound(String name)
	{
		if (this.found.containsKey(name))
		{
			if (this.found.get(name)) this.clog.log("Skipping " + name + "; already found", this);
			return false;
		}
		this.found.put(name, false);
		return true;
	}

	public File getFileOfPlugin(Class<? extends C> c)
	{
		return this.files.get(c);
	}
}

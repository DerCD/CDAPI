package com.dercd.bukkit.cdapi.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation to show which events with which priority to register to Bukkit
 * 
 * @author DerCD
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface CDPluginEvent
{
	/**
	 * @return A boolean which defines if this method should ignore the
	 *         cancel-state of an event
	 * @default false
	 */
	boolean ignoreCancelled() default false;

	/**
	 * @return An int from 0 to 12000 specifying both the Bukkit- and the intern
	 *         priority.
	 *         <p>
	 *         Higher numbers override.
	 *         <p>
	 *         List which ranges equals which EventPrioritys
	 *         <p>
	 *         0 - 2000: MONITOR
	 *         <p>
	 *         2001 - 4000: HIGHEST
	 *         <p>
	 *         4001 - 6000: HIGH
	 *         <p>
	 *         6001 - 8000: NORMAL
	 *         <p>
	 *         8001 - 10000: LOW
	 *         <p>
	 *         10001 - 12000: LOWEST
	 *         <p>
	 * @default 10000
	 */
	int priority() default 10000;

	/**
	 * @return A boolean which defines if this event should be registered after
	 *         starting the Server
	 * @default false
	 */
	boolean lateRegister() default false;
}

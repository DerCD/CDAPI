package com.dercd.bukkit.cdapi.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.bukkit.plugin.Plugin;

/**
 * Annotation to show the (soft-)depends of Bukkit-Plugins of this CDPlugin
 * 
 * @author CD
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface CDPluginDepend
{
	/**
	 * @return A list of classes extending from Plugin
	 *         <p>
	 *         this CDPlugin needs
	 */
	Class<? extends Plugin>[] depends();

	/**
	 * @return A list of names defined in the plugin.yml
	 *         <p>
	 *         this CDPlugin needs for full functionality
	 */
	String[] softdepends();
}

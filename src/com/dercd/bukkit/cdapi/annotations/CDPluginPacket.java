package com.dercd.bukkit.cdapi.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation to show which packets to register with ProtocolLib
 * 
 * @author DerCD
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface CDPluginPacket
{
	/**
	 * An int from 0 to 12000 specifying both the ProtocolLib- and the
	 *         intern priority.
	 *         <p>
	 * Higher numbers override.<p>
	 * List which ranges equals which ListenerPrioritys
	 *         <p>
	 * 0 - 2000: MONITOR
	 *         <p>
	 * 2001 - 4000: HIGHEST
	 *         <p>
	 * 4001 - 6000: HIGH
	 *         <p>
	 * 6001 - 8000: NORMAL
	 *         <p>
	 * 8001 - 10000: LOW
	 *         <p>
	 * 10001 - 12000: LOWEST
	 *         <p>
	 * @default 10000
	 */
	int priority() default 10000;

	/**
	 * @return A list of packettypes with the syntax '<(s|c) defining if this is
	 *         a Server- or Client-packet><Packet-name in lower-case>'
	 */
	String[] types();

	/**
	 * @return A boolean which defines if this method should ignore the
	 *         cancel-state of a packet
	 * @default false
	 */
	boolean ignoreCancelled() default false;

	/**
	 * @return A boolean which defines if this packet should be registered after
	 *         starting the Server
	 * @default false
	 */
	boolean lateRegister() default false;
}

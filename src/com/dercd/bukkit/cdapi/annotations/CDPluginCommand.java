package com.dercd.bukkit.cdapi.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation to show which commands with which permission to register to Bukkit
 * 
 * @author DerCD
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface CDPluginCommand
{
	/**
	 * @return A list of Strings with the syntax '<command> [permission] [notify
	 *         if not enough permission (0|1)]
	 */
	String[] commands();

	/**
	 * @return An int from 0 to 12000 specifying the intern priority.
	 *         <p>
	 *         0 is the lowest
	 * @default 10000
	 */
	int priority() default 10000;
	/**
	 * 
	 * @return A bool defining if the method should called<p>if the CommandEvent was cancelled
	 * @default false
	 */
	boolean ignoreCancelled() default false;
}

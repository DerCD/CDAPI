package com.dercd.bukkit.cdapi;

import java.security.AccessControlException;
import java.util.Map;

import org.bukkit.event.Listener;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.Plugin;

import com.dercd.bukkit.cdapi.exceptions.CDPluginNotFoundException;
import com.dercd.bukkit.cdapi.tools.Tools.Data;
import com.dercd.bukkit.plugins.Man.ManEntry;
import com.google.common.collect.ImmutableMap;

/**
 * Abstract plugin-class all Plugins have to extend from
 * 
 * @author DerCD
 */
public abstract class CDPlugin implements Listener
{
	private ImmutableMap<Class<? extends Plugin>, Plugin> depend;
	private ImmutableMap<String, Plugin> softdepend;
	private ImmutableMap<Class<? extends CDPlugin>, CDPlugin> cddepend;
	private ImmutableMap<String, CDPlugin> cdsoftdepend;
	protected PluginHandler handler;
	boolean isLoaded = false;
	boolean isEnabled = false;
	protected ThreadGroup pluginThreadGroup;
	
	public CDPlugin(PluginHandler handler)
	{
		this.handler = handler;
		this.pluginThreadGroup = new ThreadGroup(handler.getThreadGroup(), getName());
	}
	
	/**
	 * @return The directorys this plugin need. If not overridden it's null
	 */
	public String[] getDirectorys()
	{
		return null;
	}
	
	/**
	 * @return The directory all CDPlugins are in
	 */
	public static final String getDir()
	{
		return "./plugins/CDAPI/";
	}
	
	/**
	 * @return The permissions of this plugin. If not overridden it's an empty Array
	 */
	public Permission[] getPermissions()
	{
		return new Permission[0];
	}

	/**
	 * @return The version of this plugin. If not overridden it's 0.0
	 */
	public String getVersion()
	{
		return "0.0";
	}
	
	/**
	 * @return The author of this plugin. If not overridden it's null
	 */
	public String getAuthor()
	{
		return null;
	}
	
	/**
	 * @return The PluginHandler
	 */
	public final PluginHandler getHandler()
	{
		return this.handler;
	}

	/**
	 * @return The mans this plugin has. If not overridden it's null
	 */
	public ManEntry[] man()
	{
		return null;
	}
	
	/**
	 * @return Name of the plugin. If not overridden its classname
	 */
	public String getName()
	{
		return getClass().getSimpleName();
	}

	final void setDepend_DO_NOT_USE(ImmutableMap<Class<? extends Plugin>, Plugin> depend)
	{
		if (!Data.checkSamePackage()) throw new AccessControlException("Only the CDAPI is allowed to set this field");
		this.depend = depend;
	}
	final void setSoftDepend_DO_NOT_USE(ImmutableMap<String, Plugin> softdepend)
	{
		if (!Data.checkSamePackage()) throw new AccessControlException("Only the CDAPI is allowed to set this field");
		this.softdepend = softdepend;
	}
	final void setCDDepend_DO_NOT_USE(ImmutableMap<Class<? extends CDPlugin>, CDPlugin> cddepend)
	{
		if (!Data.checkSamePackage()) throw new AccessControlException("Only the CDAPI is allowed to set this field");
		this.cddepend = cddepend;
	}
	final void setCDSoftDepend_DO_NOT_USE(ImmutableMap<String, CDPlugin> cdsoftdepend)
	{
		if (!Data.checkSamePackage()) throw new AccessControlException("Only the CDAPI is allowed to set this field");
		this.cdsoftdepend = cdsoftdepend;
	}

	/**
	 * @return ImmutableMap with all Bukkit-Plugin depend classes
	 */
	public Map<Class<? extends Plugin>, Plugin> getDepends()
	{
		return this.depend;
	}
	/**
	 * @return ImmutableMap with all Bukkit-Plugin soft-depend names
	 */
	public Map<String, Plugin> getSoftDepends()
	{
		return this.softdepend;
	}

	/**
	 * @param c Class extending Plugin or CDPlugin
	 * @return The Bukkit- or CDPlugin which is listed as dependency
	 * @throws CDPluginNotFoundException if the class was not listed as
	 *         dependency
	 */
	@SuppressWarnings("unchecked")
	public <P> P getDepend(Class<P> c) throws CDPluginNotFoundException
	{
		if (this.depend.containsKey(c)) return (P) this.depend.get(c);
		if (this.cddepend.containsKey(c)) return (P) this.cddepend.get(c);
		throw new CDPluginNotFoundException(c.getSimpleName());
	}
	/**
	 * @param s Name of an Bukkit-Plugin listed as soft-dependency
	 * @return The Bukkit-Plugin if listed. Otherwise null
	 */
	public Plugin getSoftDepend(String s)
	{
		return this.softdepend.get(s);
	}
	/**
	 * @param s Name of an CDPlugin listed as soft-dependency
	 * @return The CDPlugin if listed. Otherwise null
	 */
	public CDPlugin getCDSoftDepend(String s)
	{
		return this.cdsoftdepend.get(s);
	}
	/**
	 * @param s Name of an Bukkit-Plugin listed as soft-dependency
	 * @return True if the given CDPlugin is available
	 */
	public boolean hasSoftDepend(String s)
	{
		return this.softdepend.get(s) != null;
	}
	/**
	 * @param s Name of an CDPlugin listed as soft-dependency
	 * @return True if the given CDPlugin is available
	 */
	public boolean hasCDSoftDepend(String s)
	{
		return this.cdsoftdepend.get(s) != null;
	}
	
	/**
	 * 
	 * @return The ThreadGroup of this CDPlugin
	 */
	public ThreadGroup getPluginThreadGroup()
	{
		return this.pluginThreadGroup;
	}
	
	public final boolean isLoaded()
	{
		return this.isLoaded;
	}
	
	public final boolean isEnabled()
	{
		return this.isEnabled;
	}
	
	public ErrorAction getErrorAction()
	{
		return ErrorAction.NONE;
	}
	
	public static enum ErrorAction
	{
		NONE,
		DISABLE,
		SHUTDOWN,
		KILL
	}
	

}

package com.dercd.bukkit.cdapi.listener.register;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.Messenger;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.annotations.CDPluginMessage;
import com.dercd.bukkit.cdapi.listener.PluginMessageListener;
import com.dercd.bukkit.cdapi.listener.objects.CDEventObject;
import com.dercd.bukkit.cdapi.listener.objects.CDListenerObject;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.Tools.Var;
import com.dercd.bukkit.cdapi.wrapper.PluginMessage;

public class PluginMessageRegister
{
	CDAPI cdapi;
	PluginMessageListener pmlistener;
	Log clog;
	Messenger m = Bukkit.getMessenger();

	public PluginMessageRegister(CDAPI cdapi)
	{
		this.cdapi = cdapi;
		this.pmlistener = cdapi.getHandler().getPluginMessageListener();
		this.clog = cdapi.getHandler().getCLog();
	}

	public void searchChannels()
	{
		this.clog.log("Searching PluginMessages", this);
		List<CDListenerObject> list;
		CDPluginMessage an;
		String channel;
		for (CDPlugin cdp : this.cdapi.getHandler().plugins.values())
			for (Method m : cdp.getClass().getMethods())
			{
				if ((an = m.getAnnotation(CDPluginMessage.class)) == null) continue;
				channel = an.channel();
				if ((list = this.pmlistener.registered.get(channel)) == null) list = new ArrayList<CDListenerObject>();
				list.add(new CDEventObject(cdp, m, an.priority(), an.lateRegister(), an.ignoreCancelled()));
				this.pmlistener.registered.put(channel, list);
				this.clog.log("Registering Channel " + channel + " to " + cdp.getClass().getSimpleName() + " with lateRegister = " + an.lateRegister() + ", priority = " + an.priority() + ", ignoreCancelled = " + an.ignoreCancelled(), this);
			}
		sortCalls();
		this.clog.log("Done", this);
	}
	
	public void searchChannels(CDPlugin cdp)
	{
		this.clog.log("Searching PluginMessages for " + cdp.getName(), this);
		List<CDListenerObject> list;
		CDPluginMessage an;
		String channel;
		for (Method m : cdp.getClass().getMethods())
		{
			if ((an = m.getAnnotation(CDPluginMessage.class)) == null) continue;
			channel = an.channel();
			if ((list = this.pmlistener.registered.get(channel)) == null) list = new ArrayList<CDListenerObject>();
			list.add(new CDEventObject(cdp, m, an.priority(), an.lateRegister(), an.ignoreCancelled()));
			this.pmlistener.registered.put(channel, list);
			this.clog.log("Registering Channel " + channel + " to " + cdp.getClass().getSimpleName() + " with lateRegister = " + an.lateRegister() + ", priority = " + an.priority() + ", ignoreCancelled = " + an.ignoreCancelled(), this);
		}
		sortCalls();
		this.clog.log("Done", this);
	}
	
	public void unregisterMessageChannels()
	{
		this.clog.log("Unregistering all CDAPI PluginMessageChannels", this);
		this.m.unregisterIncomingPluginChannel(this.cdapi);
		this.clog.log("All PluginMessageChannels unregistered", this);
	}

	public void registerPluginMessageChannels(boolean lateRegister)
	{
		this.clog.log("Registering PluginMessageChannels with LateRegister = " + lateRegister, this);
		Set<String> toRegister = new HashSet<String>();
		for (String s : this.pmlistener.registered.keySet())
			for (CDListenerObject o : this.pmlistener.registered.get(s))
				if (((CDEventObject) o).getLateRegistered() == lateRegister)
				{
					toRegister.add(s);
					break;
				}
		registerPluginMessageChannels(this.pmlistener.registered.keySet(), lateRegister);
		this.clog.log("All PluginMessageChannels with LateRegister = " + lateRegister + " registered", this);
	}

	private void registerPluginMessageChannels(Collection<String> channels, boolean lateRegister)
	{
		for (String channel : channels)
			registerPluginMessageChannel(channel, lateRegister);
	}

	private void registerPluginMessageChannel(String channel, boolean lateRegister)
	{
		this.m.registerIncomingPluginChannel(this.cdapi, channel, getListener(channel, lateRegister));
		this.clog.log("Registered PluginMessageChannel " + channel + " to Bukkit", this);
	}

	public void sortCalls()
	{
		this.clog.log("Sorting PluginMessageCalls by Priority", this);
		Var.sortCalls(this.pmlistener.registered);
		this.clog.log("All PluginMessages with Priority between 0 and 12000 sorted", this);
	}

	private org.bukkit.plugin.messaging.PluginMessageListener getListener(final String channel, final boolean lateRegister)
	{
		return new org.bukkit.plugin.messaging.PluginMessageListener()
		{
			@Override
			public void onPluginMessageReceived(String c, Player p, byte[] d)
			{
				if (!c.equals(channel)) return;
				try
				{
					PluginMessageRegister.this.pmlistener.onPluginMessage(new PluginMessage(c, p, d), lateRegister);
				}
				catch (Exception x)
				{
					PluginMessageRegister.this.clog.printException(x);
				}
			}
		};
	}

	public void unregisterPlugin(CDPlugin cdp)
	{
		PluginHandler.unregisterPlugin(cdp, this.pmlistener.registered);
	}
}

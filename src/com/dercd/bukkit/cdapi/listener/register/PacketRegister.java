package com.dercd.bukkit.cdapi.listener.register;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.annotations.CDPluginPacket;
import com.dercd.bukkit.cdapi.listener.PacketListener;
import com.dercd.bukkit.cdapi.listener.objects.CDListenerObject;
import com.dercd.bukkit.cdapi.listener.objects.CDPacketObject;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class PacketRegister
{
	CDAPI cdapi;
	PacketListener plistener;
	Log clog;
	ProtocolManager pm = ProtocolLibrary.getProtocolManager();

	public PacketRegister(CDAPI cdapi)
	{
		this.cdapi = cdapi;
		this.plistener = cdapi.getHandler().getPacketListener();
		this.clog = cdapi.getHandler().getCLog();
	}

	public void searchPackets()
	{
		this.clog.log("Searching Packets", this);
		List<CDListenerObject> list;
		CDPluginPacket an;
		char c;
		for (CDPlugin cdp : this.cdapi.getHandler().plugins.values())
			for (Method m : cdp.getClass().getMethods())
			{
				if ((an = m.getAnnotation(CDPluginPacket.class)) == null) continue;
				for (String type : an.types())
				{
					if (type.length() < 2 || ((c = type.charAt(0)) != 's' && c != 'c'))
					{
						this.clog.log("PacketTypes have to have a 's' or an 'c' on beginning to identify if it's a Server or Client Packet. " + type + " is unidentifyed and would not be registered", this);
						continue;
					}
					if ((list = this.plistener.registered.get(type.toLowerCase())) == null) list = new ArrayList<CDListenerObject>();
					list.add(new CDPacketObject(cdp, m, an.priority(), an.lateRegister(), an.ignoreCancelled()));
					this.plistener.registered.put(type.toLowerCase(), list);
					this.clog.log("Registering " + (c == 's' ? "Server" : "Client") + type.substring(1) + "Packet to " + cdp.getClass().getSimpleName() + " with lateRegister = " + an.lateRegister() + ", priority = " + an.priority() + ", ignoreCancelled = " + an.ignoreCancelled(), this);
				}
			}
		sortCalls();
		this.clog.log("Done", this);
	}
	
	public void searchPackets(CDPlugin cdp)
	{
		this.clog.log("Searching Packets for " + cdp.getName(), this);
		List<CDListenerObject> list;
		CDPluginPacket an;
		char c;
		for (Method m : cdp.getClass().getMethods())
		{
			if ((an = m.getAnnotation(CDPluginPacket.class)) == null) continue;
			for (String type : an.types())
			{
				if (type.length() < 2 || ((c = type.charAt(0)) != 's' && c != 'c'))
				{
					this.clog.log("PacketTypes have to have a 's' or an 'c' on beginning to identify if it's a Server or Client Packet. " + type + " is unidentifyed and would not be registered", this);
					continue;
				}
				if ((list = this.plistener.registered.get(type.toLowerCase())) == null) list = new ArrayList<CDListenerObject>();
				list.add(new CDPacketObject(cdp, m, an.priority(), an.lateRegister(), an.ignoreCancelled()));
				this.plistener.registered.put(type.toLowerCase(), list);
				this.clog.log("Registering " + (c == 's' ? "Server" : "Client") + type.substring(1) + "Packet to " + cdp.getClass().getSimpleName() + " with lateRegister = " + an.lateRegister() + ", priority = " + an.priority() + ", ignoreCancelled = " + an.ignoreCancelled(), this);
			}
		}
		sortCalls();
		this.clog.log("Done", this);
	}

	public void unregisterProtocolLibPackets()
	{
		this.clog.log("Unregistering all CDAPI Packets", this);
		this.pm.removePacketListeners(this.cdapi);
		this.clog.log("All Packets unregistered", this);
	}

	public void registerProtocolLibPackets(boolean lateRegister)
	{
		this.clog.log("Registering Packets with LateRegister = " + lateRegister + " to ProtocolLib", this);
		Map<String, List<ListenerPriority>> toRegister = new HashMap<String, List<ListenerPriority>>();
		List<ListenerPriority> l;
		CDPacketObject cdpo;
		for (String s : this.plistener.registered.keySet())
			for (CDListenerObject o : this.plistener.registered.get(s))
				if ((cdpo = (CDPacketObject) o).getLateRegistered() == lateRegister)
				{
					if (!toRegister.containsKey(s)) toRegister.put(s, new ArrayList<ListenerPriority>());
					l = toRegister.get(s);
					if (!l.contains(cdpo.getListenerPriority())) l.add(cdpo.getListenerPriority());
				}
		registerProtocolLibPackets(toRegister, lateRegister);
		this.clog.log("All Packets with LateRegister = " + lateRegister + " registered", this);
	}

	private void registerProtocolLibPackets(Map<String, List<ListenerPriority>> map, boolean lateRegister)
	{
		for (String e : map.keySet())
			for (ListenerPriority lp : map.get(e))
				registerProtocolLibPacket(e, lateRegister, lp);
	}

	public void registerProtocolLibPacket(String packet, boolean lateRegister, ListenerPriority lp)
	{
		PacketType type = getPacketType(packet);
		if (type == null)
		{
			this.clog.log("Could not register " + packet + " Packet", this);
			return;
		}
		this.pm.addPacketListener(getAdapter(type, lateRegister, lp));
		this.clog.log("Registered " + packet + " Packet to ProtocolLib", this);
	}

	public void sortCalls()
	{
		this.clog.log("Sorting PacketCalls by Priority", this);
		Var.sortCalls(this.plistener.registered);
		this.clog.log("All Packets with Priority between 0 and 12000 sorted", this);
	}

	private PacketType getPacketType(String name)
	{
		boolean serverSide = (name.charAt(0) == 's');
		name = name.substring(1);
		for (PacketType type : PacketType.values())
			if (type.isServer() != serverSide) continue;
			else if (type.name().equalsIgnoreCase(name)) return type;
		return null;
	}

	private PacketAdapter getAdapter(PacketType type, final boolean lateRegister, final ListenerPriority lp)
	{
		return new PacketAdapter(this.cdapi, lp, type)
		{
			@Override
			public void onPacketReceiving(PacketEvent e)
			{
				try
				{
					PacketRegister.this.plistener.onPacket(e, lateRegister, lp);
				}
				catch (Throwable t)
				{
					PacketRegister.this.clog.printException(t);
				}
			}

			@Override
			public void onPacketSending(PacketEvent e)
			{
				try
				{
					PacketRegister.this.plistener.onPacket(e, lateRegister, lp);
				}
				catch (Throwable t)
				{
					PacketRegister.this.clog.printException(t);
				}
			}
		};
	}

	public void unregisterPlugin(CDPlugin cdp)
	{
		PluginHandler.unregisterPlugin(cdp, this.plistener.registered);
	}
}

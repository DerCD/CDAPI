package com.dercd.bukkit.cdapi.listener.register;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.EventExecutor;
import org.bukkit.plugin.IllegalPluginAccessException;
import org.bukkit.plugin.RegisteredListener;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.annotations.CDPluginEvent;
import com.dercd.bukkit.cdapi.listener.EventListener;
import com.dercd.bukkit.cdapi.listener.objects.CDEventObject;
import com.dercd.bukkit.cdapi.listener.objects.CDListenerObject;
import com.dercd.bukkit.cdapi.listener.objects.bukkit.CDEventListener;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.Tools.Var;
import com.dercd.bukkit.cdapi.tools.collection.CDEntry;

public class EventRegister
{
	CDAPI cdapi;
	Log clog;
	EventListener elistener;

	public EventRegister(CDAPI cdapi)
	{
		this.cdapi = cdapi;
		this.clog = cdapi.getHandler().getCLog();
		this.elistener = cdapi.getHandler().getEventListener();
	}

	public void searchEvents()
	{
		this.clog.log("Searching Events", this);
		List<CDListenerObject> list;
		String name;
		CDPluginEvent cdpe;
		for (CDPlugin cdp : this.cdapi.getHandler().plugins.values())
			for (Method m : cdp.getClass().getMethods())
			{
				if ((cdpe = m.getAnnotation(CDPluginEvent.class)) == null) continue;
				name = m.getParameterTypes()[0].getName();
				if ((list = this.elistener.registered.get(name)) == null) list = new ArrayList<CDListenerObject>();
				list.add(new CDEventObject(cdp, m, cdpe.priority(), cdpe.lateRegister(), cdpe.ignoreCancelled()));
				this.elistener.registered.put(name, list);
				this.clog.log("Registering Event " + name + " to " + cdp.getClass().getSimpleName() + " with lateRegister = " + cdpe.lateRegister() + ", priority = " + cdpe.priority() + ", ignoreCancelled = " + cdpe.ignoreCancelled(), this);
			}
		sortCalls();
		this.clog.log("Done", this);
	}
	
	public void searchEvents(CDPlugin cdp)
	{
		this.clog.log("Searching Events for " + cdp.getName(), this);
		List<CDListenerObject> list;
		String name;
		CDPluginEvent an;
		for (Method m : cdp.getClass().getMethods())
		{
			if ((an = m.getAnnotation(CDPluginEvent.class)) == null) continue;
			name = m.getParameterTypes()[0].getName();
			if ((list = this.elistener.registered.get(name)) == null) list = new ArrayList<CDListenerObject>();
			list.add(new CDEventObject(cdp, m, an.priority(), an.lateRegister(), an.ignoreCancelled()));
			this.elistener.registered.put(name, list);
			this.clog.log("Registering Event " + name + " to " + cdp.getClass().getSimpleName() + " with lateRegister = " + an.lateRegister() + ", priority = " + an.priority() + ", ignoreCancelled = " + an.ignoreCancelled(), this);
		}
		sortCalls();
		this.clog.log("Done", this);
	}
	
	private void sortCalls()
	{
		this.clog.log("Sorting EventCalls by Priority", this);
		Var.sortCalls(this.elistener.registered);
		this.clog.log("All Events with Priority between 0 and 12000 sorted", this);
	}

	public void unregisterBukkitEvents()
	{
		this.clog.log("Unregister all CDAPI Events", this);
		HandlerList.unregisterAll(this.elistener);
		this.clog.log("All Events unregistered", this);
	}

	public void registerBukkitEvents(boolean lateRegister)
	{
		this.clog.log("Registering Events with LateRegister = " + lateRegister + " to Bukkit", this);
		Map<String, List<EventPriority>> toRegister = new HashMap<String, List<EventPriority>>();
		List<EventPriority> l;
		CDEventObject cdeo;
		for (String s : this.elistener.registered.keySet())
			for (CDListenerObject o : this.elistener.registered.get(s))
				if ((cdeo = (CDEventObject) o).getLateRegistered() == lateRegister)
				{
					if (!toRegister.containsKey(s)) toRegister.put(s, new ArrayList<EventPriority>());
					if (!(l = toRegister.get(s)).contains(cdeo.getBukkitPriority())) l.add(cdeo.getBukkitPriority());
				}
		registerBukkitEvents(toRegister, lateRegister);
		this.clog.log("All Events with LateRegister = " + lateRegister + " registered", this);
	}

	private void registerBukkitEvents(Map<String, List<EventPriority>> map, boolean lateRegister)
	{
		for (String event : map.keySet())
			for (EventPriority ep : map.get(event))
				registerBukkitEvent(event, lateRegister, ep);
	}

	private void registerBukkitEvent(String event, boolean lateRegister, EventPriority priority)
	{
		Entry<Class<? extends Event>, RegisteredListener> entry = getRegisteredListener(event, lateRegister, priority);
		if (entry == null)
		{
			this.clog.log("Could not register " + event, this);
			return;
		}
		getEventListeners(getRegistrationClass(entry.getKey())).register(entry.getValue());
		this.clog.log("Registered " + event + " to Bukkit", this);
	}

	private HandlerList getEventListeners(Class<? extends Event> type)
	{
		try
		{
			Method method = getRegistrationClass(type).getDeclaredMethod("getHandlerList", new Class[0]);
			method.setAccessible(true);
			return (HandlerList) method.invoke(null, new Object[0]);
		}
		catch (Exception e)
		{
			throw new IllegalPluginAccessException(e.toString());
		}
	}

	private Class<? extends Event> getRegistrationClass(Class<? extends Event> clazz)
	{
		try
		{
			clazz.getDeclaredMethod("getHandlerList");
			return clazz;
		}
		catch (NoSuchMethodException e)
		{
			if (clazz.getSuperclass() != null && !clazz.getSuperclass().equals(Event.class) && Event.class.isAssignableFrom(clazz.getSuperclass())) return getRegistrationClass(clazz.getSuperclass().asSubclass(Event.class));
			throw new IllegalPluginAccessException("Unable to find handler list for event " + clazz.getName());
		}
	}

	private Entry<Class<? extends Event>, RegisteredListener> getRegisteredListener(String eventName, boolean lateRegister, EventPriority priority)
	{
		Class<? extends Event> eventClass = getEventClass(eventName);
		if (eventClass == null) return null;
		EventExecutor executor = (listener, event) ->
		{
			try
			{
				EventRegister.this.elistener.onEvent(event, lateRegister, priority);
			}
			catch (Throwable t)
			{
				throw new EventException(t);
			}
		};
		return new CDEntry<Class<? extends Event>, RegisteredListener>(eventClass, new CDEventListener(this.cdapi.getHandler().getEventListener(), executor, this.cdapi, priority));
	}

	private Class<? extends Event> getEventClass(String name)
	{
		try
		{
			Class<?> c = Class.forName(name);
			if (Event.class.isAssignableFrom(c)) return c.asSubclass(Event.class);
			return null;
		}
		catch (ClassNotFoundException | NoClassDefFoundError x)
		{
			return null;
		}
		/*
		 * if((c = getBukkitEventClass(name)) == null) try { c =
		 * Class.forName("net.gmx.teamterrian.CD.CDAPI.events." + name); } catch
		 * (ClassNotFoundException ex) { return null; } return
		 * (Event.class.isAssignableFrom(c) ? c.asSubclass(Event.class) : null);
		 */
	}

	/*
	 * private Class<?> getBukkitEventClass(String name) { Class<?> c;
	 * for(String p : new String[] {"block", "enchantment", "entity", "hanging",
	 * "inventory", "painting", "player", "server", "vehicle", "weather",
	 * "world"}) try { c = Class.forName("org.bukkit.event." + p + "." + name);
	 * return c; } catch (ClassNotFoundException x) { continue; } return null; }
	 */
	public void unregisterPlugin(CDPlugin cdp)
	{
		PluginHandler.unregisterPlugin(cdp, this.elistener.registered);
	}
}
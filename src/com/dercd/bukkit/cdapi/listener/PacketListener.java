package com.dercd.bukkit.cdapi.listener;

import java.util.List;

import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketEvent;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.listener.objects.CDListenerObject;
import com.dercd.bukkit.cdapi.listener.objects.CDPacketObject;

public class PacketListener extends CDListener
{
	public PacketListener(PluginHandler handler)
	{
		super(handler);
	}

	public void onPacket(PacketEvent e, boolean lateRegistered, ListenerPriority lp)
	{
		String packetName = (e.isServerPacket() ? "s" : "c") + e.getPacketType().name().toLowerCase();
		CDPacketObject cdpo;
		List<CDListenerObject> cdlos = this.registered.get(packetName);
		if(cdlos != null)
			for (CDListenerObject o : cdlos)
				if ((cdpo = (CDPacketObject) o).getLateRegistered() == lateRegistered && cdpo.getListenerPriority() == lp)
					onPacket(e, cdpo);
	}

	public void onPacket(PacketEvent e, CDPacketObject cdpo)
	{
		boolean readonly = e.isReadOnly();
		try
		{
			if (readonly) e.setReadOnly(false);
			if (e.isCancelled() && !cdpo.getIgnoreCancelled()) return;
			cdpo.getMethod().invoke(cdpo.getObject(), e);
			if (readonly) e.setReadOnly(true);
		}
		catch (Throwable x)
		{
			handleException(x.getCause(), false, cdpo);
		}
	}
}

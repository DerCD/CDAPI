package com.dercd.bukkit.cdapi.listener.objects.bukkit;

import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.EventExecutor;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredListener;

public class CDEventListener extends RegisteredListener
{
	public EventExecutor myExecutor;
	public Listener myListener;

	public CDEventListener(Listener listener, EventExecutor executor, Plugin plugin, EventPriority ep)
	{
		super(listener, executor, ep, plugin, true);
		this.myExecutor = executor;
		this.myListener = listener;
	}

	@Override
	public void callEvent(Event e) throws EventException
	{
		this.myExecutor.execute(this.myListener, e);
	}
}

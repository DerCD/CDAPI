package com.dercd.bukkit.cdapi.listener.objects;

import java.lang.reflect.Method;

public class CDCommandObject extends CDListenerObject
{
	private boolean ignoreCancelled;
	
	public CDCommandObject(Object o, Method m, int priority, boolean ignoreCancelled)
	{
		super(o, m, priority);
		this.ignoreCancelled = ignoreCancelled;
	}
	
	public boolean getIgnoreCancelled()
	{
		return this.ignoreCancelled;
	}
}

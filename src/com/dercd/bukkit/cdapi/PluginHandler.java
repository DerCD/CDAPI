package com.dercd.bukkit.cdapi;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.spigotmc.SpigotConfig;

import com.dercd.bukkit.cdapi.annotations.CDInternPluginDepend;
import com.dercd.bukkit.cdapi.annotations.CDPluginDepend;
import com.dercd.bukkit.cdapi.events.CDPluginDisableEvent;
import com.dercd.bukkit.cdapi.events.CDPluginEnableEvent;
import com.dercd.bukkit.cdapi.events.CDPluginLoadEvent;
import com.dercd.bukkit.cdapi.events.CDServerStartedEvent;
import com.dercd.bukkit.cdapi.exceptions.CDGException;
import com.dercd.bukkit.cdapi.exceptions.CDPluginNotFoundException;
import com.dercd.bukkit.cdapi.exceptions.Handleable;
import com.dercd.bukkit.cdapi.listener.CommandListener;
import com.dercd.bukkit.cdapi.listener.EventListener;
import com.dercd.bukkit.cdapi.listener.PacketListener;
import com.dercd.bukkit.cdapi.listener.PluginMessageListener;
import com.dercd.bukkit.cdapi.listener.objects.CDListenerObject;
import com.dercd.bukkit.cdapi.listener.register.CommandRegister;
import com.dercd.bukkit.cdapi.listener.register.EventRegister;
import com.dercd.bukkit.cdapi.listener.register.PacketRegister;
import com.dercd.bukkit.cdapi.listener.register.PluginMessageRegister;
import com.dercd.bukkit.cdapi.tools.DynamicClassLoader;
import com.dercd.bukkit.cdapi.tools.DynamicDependencyClassLoader;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.minecraft.NameFetcher;
import com.dercd.bukkit.cdapi.tools.minecraft.UUIDFetcher;
import com.dercd.bukkit.plugins.EMStop;
import com.google.common.collect.ImmutableMap;

/**
 * Class which handles all actions with CDPlugins
 * 
 * @author DerCD
 */
public class PluginHandler
{
	private CDAPI cdapi;
	protected Logger log = Logger.getLogger("Minecraft");
	protected Log clog;
	protected EventListener elistener;
	protected PacketListener plistener;
	protected CommandListener clistener;
	protected PluginMessageListener pmlistener;
	protected boolean isLoaded = false;
	protected boolean isEnabled = false;
	protected boolean isInitialized = false;
	protected DynamicClassLoader<CDPlugin> classLoader;
	protected NameFetcher nameFetcher;
	protected UUIDFetcher uuidFetcher;
	public Map<String, Class<? extends CDPlugin>> pluginClasses = new HashMap<String, Class<? extends CDPlugin>>();
	public Map<Class<? extends CDPlugin>, CDPlugin> plugins = new HashMap<Class<? extends CDPlugin>, CDPlugin>();
	protected CommandRegister cRegister;
	protected EventRegister eRegister;
	protected PacketRegister pRegister;
	protected PluginMessageRegister pmRegister;
	protected double[] startTimes = new double[3];
	protected ThreadGroup threadGroup = new ThreadGroup("CDAPI-ThreadGroup");
	protected boolean spigot = false, bungeeCord = false;
	
	protected PluginHandler(CDAPI cdapi)
	{
		this.cdapi = cdapi;
		initialize();
		getSpigot();
	}

	/**
	 * @return The CDAPI-Object associated with this handler
	 */
	public CDAPI getMain()
	{
		return this.cdapi;
	}

	/**
	 * Initialize one CDPlugin
	 * 
	 * @param c Class extending CDPlugin to initialize
	 */
	public void initialize(Class<? extends CDPlugin> c) throws IllegalArgumentException, ReflectiveOperationException, SecurityException
	{
		if (!this.plugins.containsKey(c)) return;
		if (this.plugins.get(c) != null) return;
		CDPlugin cdp = c.getConstructor(PluginHandler.class).newInstance(this);
		this.plugins.put(c, cdp);
		this.pluginClasses.put(cdp.getName(), c);
	}

	private void initialize()
	{
		this.clog = new Log(CDPlugin.getDir() + "logs", this);
		this.elistener = new EventListener(this);
		this.plistener = new PacketListener(this);
		this.clistener = new CommandListener(this);
		this.pmlistener = new PluginMessageListener(this);
	}
	
	private void getSpigot()
	{
		try
		{
			this.bungeeCord = SpigotConfig.bungee;
			this.spigot = true;
		}
		catch(Throwable t) { }
	}
	
	private void initializeAfterConstructed()
	{
		this.nameFetcher = new NameFetcher();
		this.uuidFetcher = new UUIDFetcher();
		this.nameFetcher.setUUIDFetcher(this.uuidFetcher);
		this.uuidFetcher.setNameFetcher(this.nameFetcher);
		this.cRegister = new CommandRegister(this.cdapi);
		this.eRegister = new EventRegister(this.cdapi);
		this.pRegister = new PacketRegister(this.cdapi);
		this.pmRegister = new PluginMessageRegister(this.cdapi);
		this.isInitialized = true;
	}

	private void uninitialize()
	{
		unregisterPermissions();
		this.cRegister.unregisterBukkitCommands();
		this.pRegister.unregisterProtocolLibPackets();
		this.eRegister.unregisterBukkitEvents();
		this.pmRegister.unregisterMessageChannels();
		this.plugins.clear();
		this.pluginClasses.clear();
		Bukkit.getScheduler().cancelTasks(this.cdapi);
		this.elistener = null;
		this.plistener = null;
		this.clistener = null;
		this.pmlistener = null;
		this.isEnabled = false;
		this.cRegister = null;
		this.pRegister = null;
		this.eRegister = null;
		DynamicClassLoader.createdClassLoader = null;
		DynamicDependencyClassLoader.excluded.clear();
		System.gc();
		this.isInitialized = false;
	}

	private void uninitialize(CDPlugin cdp)
	{
		this.plugins.put(cdp.getClass(), null);
		cdp = null;
		System.gc();
	}

	private void deregister(CDPlugin cdp)
	{
		this.cRegister.unregisterPlugin(cdp);
		this.pRegister.unregisterPlugin(cdp);
		this.eRegister.unregisterPlugin(cdp);
		this.pmRegister.unregisterPlugin(cdp);
	}

	/**
	 * Reloads one CDPlugin
	 * 
	 * @param cdp CDPlugin to reload
	 * @throws ReflectiveOperationException 
	 */
	public void reload(CDPlugin cdp) throws IllegalArgumentException, SecurityException, ReflectiveOperationException
	{
		String name = cdp.getName();
		this.log.info("[CDAPI] Disabling " + name);
		disable(cdp);
		this.log.info("[CDAPI] Initializing " + name);
		initialize(cdp.getClass());
//		cdp = cdp.getClass().getConstructor(PluginHandler.class).newInstance(this);
//		this.plugins.put(cdp.getClass(), cdp);
		cdp = this.plugins.get(cdp.getClass());
		this.log.info("[CDAPI] Loading " + name);
		load(cdp);
		this.log.info("[CDAPI] Enabling " + name);
		enable(cdp);
		this.log.info("[CDAPI] Reload done " + name);
	}

	/**
	 * Reloads all registered CDPlugins
	 */
	public void reload()
	{
		this.log.info("[CDAPI] Disabling");
		disable();
		this.log.info("[CDAPI] Initializing");
		initialize();
		this.log.info("[CDAPI] Loading");
		load();
		this.log.info("[CDAPI] Enabling");
		enable();
		this.log.info("[CDAPI] Reload done");
	}

	/**
	 * Loads one CDPlugin
	 * 
	 * @param cdp The CDPlugin to load
	 */
	public void load(CDPlugin cdp)
	{
		if (cdp.isLoaded) return;
		registerPermissions(cdp);
		this.cRegister.searchCommands(cdp, false);
		this.eRegister.searchEvents(cdp);
		this.pRegister.searchPackets(cdp);
		this.pmRegister.searchChannels(cdp);
		setDependPlugins(cdp, Bukkit.getPluginManager().getPlugins());
		CDPluginLoadEvent e = new CDPluginLoadEvent();
		this.clog.log("Loading plugin " + cdp.getName(), this);
		this.elistener.callEvent(e, cdp.getClass());
		if(e.isSuccessSet(cdp) || e.getSuccess(cdp))
			this.clog.log("Enabling of plugin " + cdp.getName() + " successfull", this);
		else
			this.clog.log("Enabling of plugin " + cdp.getName() + " unsuccessfull", this);
		cdp.isLoaded = true;
		this.clog.log("Plugin " + cdp.getName() + " loaded", this);
	}

	/**
	 * Loads all registered CDPlugins
	 */
	public void load()
	{
		long currentTime = System.currentTimeMillis();
		initializeAfterConstructed();
		this.clistener.registerAReceive();
		try
		{
			loadClasses();
		}
		catch (Exception x)
		{
			this.clog.printException(x);
			return;
		}
		doDirectorys();
		this.eRegister.searchEvents();
		this.eRegister.registerBukkitEvents(false);
		this.pRegister.searchPackets();
		this.pRegister.registerProtocolLibPackets(false);
		this.cRegister.searchCommands(false);
		this.pmRegister.searchChannels();
		this.pmRegister.registerPluginMessageChannels(false);
		registerPermissions();
		setDependPlugins();
		this.clog.log("Calling LoadEvent", this);
		CDPluginLoadEvent e = new CDPluginLoadEvent();
		this.elistener.onEvent(e);
		int failed = 0;
		for (boolean success : e.getSuccess())
			if (!success) failed++;
		this.clog.log((e.getSuccess().size() - failed) + " intern Plugins catched and processed the LoadEvent successfully, " + failed + " failed", this);
		if (failed != 0) this.log.warning(failed + " Plugins failed to load");
		this.isLoaded = true;
		for(CDPlugin cdp : this.plugins.values())
			cdp.isLoaded = true;
		this.clog.log("Plugins loaded", this);
		this.startTimes[0] = System.currentTimeMillis() - currentTime;
		this.clog.log("Needed " + this.startTimes[0] / 1000 + " seconds to load", this);
	}

	/**
	 * Enables one CDPlugin
	 * 
	 * @param cdp The CDPlugin to enable
	 */
	public void enable(CDPlugin cdp)
	{
		if (cdp.isEnabled || !cdp.isLoaded) return;
		CDPluginEnableEvent e = new CDPluginEnableEvent();
		this.clog.log("Enabling plugin " + cdp.getClass().getSimpleName(), this);
		this.elistener.callEvent(e, cdp.getClass());
		cdp.isEnabled = true;
		CDServerStartedEvent e2 = new CDServerStartedEvent();
		this.elistener.callEvent(e2, cdp.getClass());
		if((!e.isSuccessSet(cdp) || e.getSuccess(cdp)) && (e2.isSuccessSet(cdp) || e2.getSuccess(cdp)))
			this.clog.log("Enabling of plugin " + cdp.getName() + " successfull", this);
		else
			this.clog.log("Enabling of plugin " + cdp.getName() + " unsuccessfull", this);
		this.clog.log("Plugin " + cdp.getClass().getSimpleName() + " enabled", this);
	}

	/**
	 * Enables all registered CDPlugins
	 */
	public void enable()
	{
		long currentTime = System.currentTimeMillis();
		if (!this.isInitialized)
		{
			initialize();
			initializeAfterConstructed();
		}
		this.clog.log("Enabling", this);
		this.clog.log("Calling EnableEvent", this);
		CDPluginEnableEvent e = new CDPluginEnableEvent();
		this.elistener.onEvent(e);
		int failed = 0;
		for (boolean success : e.getSuccess())
			if (!success) failed++;
		this.clog.log((e.getSuccess().size() - failed) + " Plugins catched and processed the EnableEvent successfully, " + failed + " failed", this);
		if (failed != 0) this.log.warning(failed + " Plugins failed to enable");
		Bukkit.getScheduler().scheduleSyncDelayedTask(this.cdapi, () ->
		{
			PluginHandler.this.eRegister.registerBukkitEvents(true);
			PluginHandler.this.pRegister.registerProtocolLibPackets(true);
			PluginHandler.this.pmRegister.registerPluginMessageChannels(true);
			PluginHandler.this.cRegister.registerBukkitCommands();
			serverStarted();
		}, 0);
		this.clog.log("Plugins enabled", this);
		this.isEnabled = true;
		for(CDPlugin cdp : this.plugins.values())
			cdp.isEnabled = true;
		this.startTimes[1] = System.currentTimeMillis() - currentTime;
		this.clog.log("Needed " + this.startTimes[1] / 1000 + " seconds to enable", this);
	}

	void serverStarted()
	{
		this.clog.log("Calling ServerStartedEvent", this);
		CDServerStartedEvent e = new CDServerStartedEvent();
		this.elistener.onEvent(e);
		int failed = 0;
		for (boolean success : e.getSuccess())
			if (!success) failed++;
		this.clog.log((e.getSuccess().size() - failed) + " Plugins catched and processed the ServerStartedEvent successfully, " + failed + " failed", this);
	}
	
	/**
	 * 
	 * @return If this server runs Spigot
	 */
	public boolean isSpigot()
	{
		return this.spigot;
	}
	
	/**
	 * 
	 * @return If this server uses BungeeCord
	 */
	public boolean isBungeeCord()
	{
		return this.bungeeCord;
	}
	
	/**
	 * @param name The name of the CDPlugin. If an registered CDPlugin is null
	 *        its class-name, otherwise the name given by the
	 *        CDPlugin.getName()-method
	 * @return The CDPlugin if found, otherwise null
	 * @throws CDPluginNotFoundException 
	 */
	public CDPlugin getPlugin(String name) throws CDPluginNotFoundException
	{
		CDPlugin cdp = null;
		boolean found = false;
		for(CDPlugin c : this.plugins.values())
			if(c != null)
				if(c.getName().equals(name))
					return c;
				else if(c.getName().equalsIgnoreCase(name))
				{
					cdp = c;
					found = true;
				}
		if(found) return cdp;
		for (Class<? extends CDPlugin> c : this.plugins.keySet())
			if(c.getSimpleName().equals(name) || c.getName().equals(name))
				return this.plugins.get(c);
			else if(c.getSimpleName().equalsIgnoreCase(name) || c.getName().equalsIgnoreCase(name))
			{
				cdp = this.plugins.get(c);
				found = true;
			}
		if(found) return cdp;
		throw new CDPluginNotFoundException(name);
	}
	
	public Class<? extends CDPlugin> getPluginClass(String name) throws CDPluginNotFoundException
	{
		Class<? extends CDPlugin> clazz;
		clazz = this.pluginClasses.get(name);
		if(clazz != null) return clazz;
		for(String pluginName : this.pluginClasses.keySet())
			if(name.equalsIgnoreCase(pluginName))
				return this.pluginClasses.get(pluginName);
		for(Class<? extends CDPlugin> c : this.pluginClasses.values())
			if(c != null)
				if(c.getSimpleName().equals(name) || c.getName().equals(name))
					return c;
				else if(c.getSimpleName().equalsIgnoreCase(name) || c.getName().equals(name))
					clazz = c;
		if(clazz != null) return clazz;
		throw new CDPluginNotFoundException(name);
	}

	/**
	 * @param clazz The class of an registered CDPlugin
	 * @return The CDPlugin if found, otherwise null
	 */
	@SuppressWarnings("unchecked")
	public <CDP> CDP getPlugin(Class<CDP> clazz)
	{
		return (CDP) this.plugins.get(clazz);
	}

	private void loadClasses() throws IllegalArgumentException, SecurityException, UnsupportedEncodingException
	{
		this.clog.log("Clearing Plugins", this);
		this.plugins.clear();
		this.clog.log("Finding Plugins and calling Constructors of it", this);
		this.classLoader = new DynamicDependencyClassLoader<CDPlugin>(this.clog);
		URL cFile = this.cdapi.getClass().getProtectionDomain().getCodeSource().getLocation();
		DynamicClassLoader.preCreatedClassLoaders.put(cFile, this.cdapi.getClass().getClassLoader());
		for (CDPlugin cdp : this.classLoader.loadFiles(new File[] { new File(CDPlugin.getDir()), new File(URLDecoder.decode(cFile.getPath(), "UTF-8")) }, CDPlugin.class, new Class<?>[] { PluginHandler.class }, this))
		{
			this.plugins.put(cdp.getClass(), cdp);
			this.pluginClasses.put(cdp.getName(), cdp.getClass());
		}
		this.clog.log("Found " + this.plugins.size() + " CDPlugins", this);
		this.clog.log("Done", this);
	}

	private void doDirectorys()
	{
		this.clog.log("Checking and creating Directorys", this);
		checkDir(CDPlugin.getDir());
		String[] paths;
		for (CDPlugin cdp : this.plugins.values())
			if ((paths = cdp.getDirectorys()) != null) for (String path : paths)
				checkDir(CDPlugin.getDir() + path);
		this.clog.log("All Directorys checked", this);
	}

	public void unregisterPermissions(CDPlugin cdp)
	{
		PluginManager manager = Bukkit.getPluginManager();
		this.clog.log("Unregister Permissions for " + cdp.getClass().getName(), this);
		for (Permission perm : cdp.getPermissions())
			manager.removePermission(perm);
	}

	private void unregisterPermissions()
	{
		this.clog.log("Unregister Permission", this);
		PluginManager manager = Bukkit.getPluginManager();
		for (CDPlugin cdp : this.plugins.values())
			if(cdp != null)
				for (Permission perm : cdp.getPermissions())
					manager.removePermission(perm);
		this.clog.log("All Permissions unregistered", this);
	}

	/**
	 * Registers all in the CDPlugin listed permissions to Bukkit
	 * 
	 * @param cdp The CDPlugin to lookup the permissions
	 */
	public void registerPermissions(CDPlugin cdp)
	{
		PluginManager manager = Bukkit.getPluginManager();
		this.clog.log("Register Permissions for " + cdp.getClass().getName(), this);
		for (Permission perm : cdp.getPermissions())
			manager.addPermission(perm);
	}

	private void registerPermissions()
	{
		this.clog.log("Register Permissions", this);
		PluginManager manager = Bukkit.getPluginManager();
		for (CDPlugin p : this.plugins.values())
			for (Permission perm : p.getPermissions())
				try
				{
					manager.addPermission(perm);
				}
				catch (Exception x)
				{
					this.clog.log("Error while registering Permission " + perm.getName(), this);
				}
		this.clog.log("All Permissions registered", this);
	}

	/**
	 * Disables one CDPlugin
	 * 
	 * @param cdp The CDPlugin to disable
	 */
	public void disable(CDPlugin cdp)
	{
		if (!cdp.isEnabled) return;
		unregisterPermissions(cdp);
		CDPluginDisableEvent e = new CDPluginDisableEvent();
		this.clog.log("Deregistering plugin " + cdp.getClass().getSimpleName(), this);
		deregister(cdp);
		this.clog.log("Plugin " + cdp.getClass().getSimpleName() + " deregistered", this);
		this.clog.log("Disabling plugin " + cdp.getClass().getSimpleName(), this);
		this.elistener.callEvent(e, cdp.getClass());
		cdp.isEnabled = false;
		this.clog.log("Plugin " + cdp.getClass().getSimpleName() + " disabled", this);
		this.clog.log("Interrupting all Threads in the plugins ThreadGroup", this);
		try
		{
			cdp.getPluginThreadGroup().interrupt();
			this.clog.log("All Threads in the ThreadGroup interrupted", this);
		}
		catch (SecurityException x) { this.clog.log("SecurityException while interrupting Threads. No Thread was interrupted", this); }
		this.clog.log("Uninitializing plugin " + cdp.getClass().getSimpleName(), this);
		uninitialize(cdp);
		cdp.isLoaded = false;
		this.clog.log("Plugin " + cdp.getClass().getSimpleName() + " uninitialized", this);
	}

	/**
	 * Disables all listed CDPlugins
	 */
	public void disable()
	{
		long currentTime = System.currentTimeMillis();
		this.clog.log("Calling DisableEvent", this);
		CDPluginDisableEvent e = new CDPluginDisableEvent();
		this.elistener.onEvent(e);
		int failed = 0;
		for (boolean success : e.getSuccess())
			if (!success) failed++;
		this.clog.log((e.getSuccess().size() - failed) + " Plugins catched and processed the DisableEvent successfully, " + failed + " failed", this);
		if (failed != 0)
		{
			this.log.warning(failed + " Plugins failed to disable");
			this.log.warning("Maybe data isn't save");
		}
		else this.clog.log("Plugins disabled", this);
		for(CDPlugin cdp : this.plugins.values())
		{
			if(cdp == null) continue;
			cdp.isLoaded = false;
			cdp.isEnabled = false;
		}
		this.clog.log("Interrupting all Threads in the CDAPI ThreadGroup", this);
		try
		{
			this.threadGroup.interrupt();
			this.clog.log("All Threads in the ThreadGroup interrupted", this);
		}
		catch (SecurityException x) { this.clog.log("SecurityException while interrupting Threads. No Thread was interrupted", this); }
		this.clog.log("Unitialize CDAPI", this);
		uninitialize();
		this.clog.log("Done", this);
		this.startTimes[2] = System.currentTimeMillis() - currentTime;
		this.clog.log("Needed " + this.startTimes[2] / 1000 + " seconds to disable", this);
		this.clog.close();
		this.clog = null;
		System.gc();
	}

	/**
	 * Checks for all listed CDPlugin the directorys given by the
	 * CDPlugin.getDirectorys()-method
	 * <p>
	 * if they existing. If not it would be created
	 * 
	 * @param path
	 */
	public void checkDir(String path)
	{
		this.clog.log("Checking Directory " + path, this);
		File f = new File(path);
		if (!f.exists())
		{
			this.clog.log("Not existing. Creating it", this);
			f.mkdirs();
		}
		else this.clog.log("Existing", this);
	}

	/**
	 * Universial method used by the listeners to unregister all listed
	 * refereces
	 * 
	 * @param cdp The CDPlugin to unregister
	 * @param map The Map to unregister from
	 */
	public static void unregisterPlugin(CDPlugin cdp, Map<String, List<CDListenerObject>> map)
	{
		List<CDListenerObject> toRemove = new ArrayList<CDListenerObject>();
		List<CDListenerObject> val;
		for (String key : new HashMap<String, List<CDListenerObject>>(map).keySet())
		{
			toRemove.clear();
			val = map.get(key);
			for (CDListenerObject o : new ArrayList<CDListenerObject>(val))
				if (o.getObject() == cdp) toRemove.add(o);
			if (toRemove.isEmpty()) continue;
			val.removeAll(toRemove);
			if (val.isEmpty()) map.remove(key);
			else map.put(key, val);
		}
	}

	private void setDependPlugins()
	{
		this.clog.log("Setting (soft-)depend plugins for CDPlugins", this);
		PluginManager pm = Bukkit.getPluginManager();
		Plugin[] bukkitPlugins = pm.getPlugins();
		for (CDPlugin cdp : this.plugins.values())
		{
			setDependPlugins(cdp, bukkitPlugins);
			setDependCDPlugins(cdp);
		}
		this.clog.log("Done", this);
	}

	private void setDependCDPlugins(CDPlugin cdp)
	{
		CDInternPluginDepend pd = cdp.getClass().getAnnotation(CDInternPluginDepend.class);
		Map<Class<? extends CDPlugin>, CDPlugin> depends = new HashMap<Class<? extends CDPlugin>, CDPlugin>();
		Map<String, CDPlugin> softdepends = new HashMap<String, CDPlugin>();
		if (pd != null)
		{
			for (Class<? extends CDPlugin> c : pd.depends())
				for (CDPlugin p : this.plugins.values())
					if (p.getClass() == c)
					{
						depends.put(c, p);
						break;
					}
			for (String s : pd.softdepends())
				for (CDPlugin p : this.plugins.values())
					if (p.getName().equals(s))
					{
						softdepends.put(s, p);
						break;
					}
		}
		cdp.setCDDepend_DO_NOT_USE(ImmutableMap.copyOf(depends));
		cdp.setCDSoftDepend_DO_NOT_USE(ImmutableMap.copyOf(softdepends));
	}

	private void setDependPlugins(CDPlugin cdp, Plugin[] plugins)
	{
		CDPluginDepend pd = cdp.getClass().getAnnotation(CDPluginDepend.class);
		Map<Class<? extends Plugin>, Plugin> depends = new HashMap<Class<? extends Plugin>, Plugin>();
		Map<String, Plugin> softdepends = new HashMap<String, Plugin>();
		if (pd != null)
		{
			for (Class<? extends Plugin> c : pd.depends())
				for (Plugin p : plugins)
					if (p.getClass() == c)
					{
						depends.put(c, p);
						break;
					}
			for (String s : pd.softdepends())
				for (Plugin p : plugins)
					if (p.getName().equals(s))
					{
						softdepends.put(s, p);
						break;
					}
		}
		cdp.setDepend_DO_NOT_USE(ImmutableMap.copyOf(depends));
		cdp.setSoftDepend_DO_NOT_USE(ImmutableMap.copyOf(softdepends));
	}
	
	public int getInitializedPlugins()
	{
		int i = 0;
		for(CDPlugin cdp : this.plugins.values())
			if(cdp != null) i++;
		return i;
	}
	
	public int getLoadedPlugins()
	{
		int i = 0;
		for(CDPlugin cdp : this.plugins.values())
			if(cdp != null && cdp.isLoaded) i++;
		return i;
	}
	
	public int getEnabledPlugins()
	{
		int i = 0;
		for(CDPlugin cdp : this.plugins.values())
			if(cdp != null && cdp.isEnabled) i++;
		return i;
	}
	
	public int getDisabledPlugins()
	{
		int i = 0;
		for(CDPlugin cdp : this.plugins.values())
			if(cdp == null) i++;
		return i;
	}
	
	/**
	 * Try to handle a CDGException
	 * 
	 * @param x The CDGException to handle
	 */
	public void throwException(CDGException x)
	{
		x.handle();
	}
	
	
	/**
	 * 
	 * @param x The exception to be handled
	 * @param wasHandled If the exception was already tried to handle
	 */
	public void handleException(Throwable x, boolean wasHandled, CDPlugin cdp)
	{
		try
		{
			if (x == null) return;
			if (wasHandled)
			{
				this.clog.printException(x);
				this.clog.error("Error while handling something. Exceptions should has been logged", this);
				performErrorAction(cdp);
			}
			else if (x instanceof Handleable) ((Handleable) x).handle();
			else handleException(x, true, cdp);
		}
		catch (Throwable ex)
		{
			handleException(ex, false, cdp);
		}
	}
	
	public void performErrorAction(CDPlugin cdp)
	{
		String msg = "An non-Handleable exception was thrown. Because of the ErrorAction of the CDPlugin ";
		switch(cdp.getErrorAction())
		{
			case DISABLE:
				this.clog.log(msg + cdp.getName() + " the plugin will now be disabled", this);
				disable(cdp);
				return;
			case KILL:
				this.clog.log(msg + cdp.getName() + " the server process will now be killed (only on Linux)", this);
				EMStop.kill();
				return;
			case NONE:
				return;
			case SHUTDOWN:
				this.clog.log(msg + cdp.getName() + " the server will now be shut down", this);
				Bukkit.shutdown();
				return;
		}
	}
	
	/**
	 * @return The Logger used by the server
	 */
	public Logger getLog()
	{
		return this.log;
	}

	/**
	 * @return The Logger used by the CDAPI
	 */
	public Log getCLog()
	{
		return this.clog;
	}

	/**
	 * @return The intern EventListener
	 */
	public EventListener getEventListener()
	{
		return this.elistener;
	}

	/**
	 * @return The intern PacketListener
	 */
	public PacketListener getPacketListener()
	{
		return this.plistener;
	}

	/**
	 * @return The intern CommandListener
	 */
	public CommandListener getCommandListener()
	{
		return this.clistener;
	}

	/**
	 * @return The intern PluginMessageListener
	 */
	public PluginMessageListener getPluginMessageListener()
	{
		return this.pmlistener;
	}

	/**
	 * @return If the Plugins are loaded
	 */
	public boolean isLoaded()
	{
		return this.isLoaded;
	}

	/**
	 * @return If the Plugins are enabled
	 */
	public boolean isEnabled()
	{
		return this.isEnabled;
	}

	/**
	 * @return If the Plugins are initialized
	 */
	public boolean isInitialized()
	{
		return this.isInitialized;
	}

	/**
	 * @return The intern ClassLoader
	 */
	public DynamicClassLoader<CDPlugin> getClassLoader()
	{
		return this.classLoader;
	}

	/**
	 * @return The intern NameFetcher
	 */
	public NameFetcher getNameFetcher()
	{
		return this.nameFetcher;
	}

	/**
	 * @return The intern UUIDFetcher
	 */
	public UUIDFetcher getUUIDFetcher()
	{
		return this.uuidFetcher;
	}

	/**
	 * @return The intern CommandRegister
	 */
	public CommandRegister getCommandRegister()
	{
		return this.cRegister;
	}

	/**
	 * @return The intern EventRegister
	 */
	public EventRegister getEventRegister()
	{
		return this.eRegister;
	}

	/**
	 * @return The intern PacketRegister
	 */
	public PacketRegister getPacketRegister()
	{
		return this.pRegister;
	}

	/**
	 * @return The intern PluginMessageRegister
	 */
	public PluginMessageRegister getPluginMessageRegister()
	{
		return this.pmRegister;
	}
	
	/**
	 * @return The CDAPI ThreadGroup
	 */
	public ThreadGroup getThreadGroup()
	{
		return this.threadGroup;
	}
}

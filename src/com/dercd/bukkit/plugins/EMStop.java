package com.dercd.bukkit.plugins;

import java.io.IOException;

import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.annotations.CDPluginCommand;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.tools.Log;

public class EMStop extends CDPlugin
{
	Log clog;

	public EMStop(PluginHandler handler)
	{
		super(handler);
		this.clog = handler.getCLog();
	}

	@Override
	public String getVersion()
	{
		return "0.1";
	}
	
	@Override
	public final String getAuthor()
	{
		return "CD";
	}
	
	@Override
	public Permission[] getPermissions()
	{
		return new Permission[] { new Permission("cd.emstop", PermissionDefault.OP) };
	}

	@CDPluginCommand(commands = { "emstop cd.emstop 1" })
	public void onCommand(CommandEvent e)
	{
		doKill();
	}

	public void doKill()
	{
		try
		{
			this.clog.log("Trying to kill this process", this);
			Runtime.getRuntime().exec("kill -9 " + getPID());
		}
		catch (Exception x)
		{
			this.clog.printException(x);
		}
	}
	
	public static void kill()
	{
		try
		{
			Runtime.getRuntime().exec("kill -9 " + getPID());
		}
		catch (IOException e)
		{
		}
	}
	
	
	private static int getPID()
	{
		String tmp = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
		tmp = tmp.split("@")[0];
		return Integer.valueOf(tmp);
	}
}

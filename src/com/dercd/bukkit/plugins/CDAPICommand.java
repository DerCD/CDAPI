package com.dercd.bukkit.plugins;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.PluginDescriptionFile;

import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.exceptions.CDPluginNotFoundException;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class CDAPICommand extends CDPlugin
{
	Log clog;
	
	public CDAPICommand(PluginHandler handler)
	{
		super(handler);
		this.clog = handler.getCLog();
	}

	@Override
	public String getVersion()
	{
		return "0.3.1";
	}
	
	@Override
	public final String getAuthor()
	{
		return "CD";
	}
	
	public void onCommand(String[] args, CommandSender sender)
	{
		if (!(sender instanceof ConsoleCommandSender) && !sender.hasPermission("cd.cdapi")) return;
		CDPlugin cdp;
		PluginDescriptionFile desc = this.handler.getMain().getDescription();
		switch (args.length)
		{
			case 0:
				sender.sendMessage("" + ChatColor.GREEN + ChatColor.BOLD + desc.getName() + " " + ChatColor.WHITE + "v" + desc.getVersion() + ChatColor.GOLD + " " + this.handler.plugins.size() + ChatColor.WHITE + " Plugins loaded");
				return;
			case 1:
				switch (args[0])
				{
					case "reload":
						this.handler.reload();
						sender.sendMessage("" + ChatColor.GREEN + ChatColor.BOLD + desc.getName() + " " + ChatColor.WHITE + " All CDPlugins reloaded");
						return;
					case "list":
						sender.sendMessage("" + ChatColor.GOLD + ChatColor.BOLD + this.handler.plugins.size() + " Plugins loaded: " + ChatColor.LIGHT_PURPLE + Var.arrToString(Var.toArray(Var.makeStringList(this.handler.plugins.keySet())), ChatColor.LIGHT_PURPLE + "", ChatColor.WHITE + ", ", 0, false, true));
						return;
					case "debug":
						sender.sendMessage("" + ChatColor.GOLD + ChatColor.BOLD + "Current debug state: " + ChatColor.LIGHT_PURPLE + (this.clog.isGlobalDebugEnabled() ? "enabled" : "disabled"));
						return;
					default:
						try { cdp = this.handler.getPlugin(args[0]); }
						catch (CDPluginNotFoundException x) { x.handle(this.handler, sender); return; }
						if(cdp == null)
						{
							sender.sendMessage("" + ChatColor.GREEN + ChatColor.BOLD + desc.getName() + ChatColor.RED + " A CDPlugin with this name wasn't found");
							return;
						}
						sender.sendMessage("" + ChatColor.GREEN + ChatColor.BOLD + desc.getName() + ChatColor.LIGHT_PURPLE + " " + cdp.getName() + ChatColor.WHITE + " v" + cdp.getVersion() + (cdp.getAuthor() == null ? "" : " by " + ChatColor.GOLD + cdp.getAuthor()));
						return;
				}
			case 2:
				switch(args[0])
				{
					case "reload":
					case "disable":
					case "initialize":
					case "load":
					case "enable":
						try { cdp = this.handler.getPlugin(args[1]); }
						catch (CDPluginNotFoundException x) { x.handle(this.handler, sender); return; }
						if (cdp == null && !args[0].equals("initialize"))
						{
							sender.sendMessage(Var.getExclamation(ChatColor.RED) + "The Plugin '" + args[1] + "' is not initialized");
							return;
						}
						try
						{
							switch (args[0])
							{
								case "reload":
									this.handler.reload(cdp);
									sender.sendMessage(Var.getExclamation(ChatColor.GREEN) + "Plugin '" + args[1] + "' reloaded");
									return;
								case "disable":
									if(!cdp.isEnabled())
									{
										sender.sendMessage(Var.getExclamation(ChatColor.RED) + "Plugin '" + args[1] + "' isn't enabled");
										return;
									}
									this.handler.disable(cdp);
									sender.sendMessage(Var.getExclamation(ChatColor.GREEN) + "Plugin '" + args[1] + "' disabled");
									return;
								case "initialize":
									if(cdp != null)
									{
										sender.sendMessage(Var.getExclamation(ChatColor.RED) + "Plugin '" + args[1] + "' is already initialized");
										return;
									}
									this.handler.initialize(this.handler.getPluginClass(args[1]));
									sender.sendMessage(Var.getExclamation(ChatColor.GREEN) + "Plugin '" + args[1] + "' initialized");
									return;
								case "load":
									if(cdp.isLoaded())
									{
										sender.sendMessage(Var.getExclamation(ChatColor.RED) + "Plugin '" + args[1] + "' is already loaded");
										return;
									}
									this.handler.load(cdp);
									sender.sendMessage(Var.getExclamation(ChatColor.GREEN) + "Plugin '" + args[1] + "' loaded");
									return;
								case "enable":
									if(cdp.isEnabled())
									{
										sender.sendMessage(Var.getExclamation(ChatColor.RED) + "Plugin '" + args[1] + "' is already enabled");
										return;
									}
									if(!cdp.isLoaded())
									{
										sender.sendMessage(Var.getExclamation(ChatColor.RED) + "Plugin '" + args[1] + "' wasn't loaded yet");
										return;
									}
									this.handler.enable(cdp);
									sender.sendMessage(Var.getExclamation(ChatColor.GREEN) + "Plugin '" + args[1] + "' enabled");
									return;
							}
						}
						catch (Exception x)
						{
							this.clog.printException(x);
							sender.sendMessage(Var.getExclamation(ChatColor.DARK_RED) + "Error while performing action to '" + args[1] + "'");
							return;
						}
						return;
					case "debug":
						boolean b;
						if(Var.ifBool(args[1]))
							b = Boolean.valueOf(args[1]);
						else
							b = !args[1].equals("0");
						if(b)
						{
							this.clog.enableGlobalDebug();
							sender.sendMessage("" + ChatColor.GOLD + ChatColor.BOLD + "GlobalDebugMode now enabled");
						}
						else
						{
							this.clog.disableGlobalDebug();
							sender.sendMessage("" + ChatColor.GOLD + ChatColor.BOLD + "GlobalDebugMode now disabled");
						}
						this.clog.log("GlobalDebugMode now " + (b ? "enabled" : "disabled"), this);
						return;
					case "perms":
					case "permissions":
						try { cdp = this.handler.getPlugin(args[1]); }
						catch (CDPluginNotFoundException x) { x.handle(this.handler, sender); return; }
						Permission[] perms = cdp.getPermissions();
						if(perms.length == 0)
							sender.sendMessage(Var.getExclamation(ChatColor.GREEN) + " Plugin '" + args[1] + "' has no permissions");
						else
						{
							sender.sendMessage(Var.getExclamation(ChatColor.GREEN) + " Permissions of Plugin '" + args[1] + "'");
							for(Permission perm : perms)
								sender.sendMessage("\t" + ChatColor.GRAY + perm.getName() + ChatColor.WHITE + " - " + ChatColor.GOLD + perm.getDefault().toString());
						}
						return;
				}
		}
	}
}
